// This file can be used to configure global preferences for Firefox
// Example: Homepage
//pref("browser.startup.homepage", "http://www.weebls-stuff.com/wab/");

pref("plugins.flashBlock.enabled", false, locked);
pref("browser.disableResetPrompt", true, locked);
pref("plugins.http_https_only", false, locked);
pref("privacy.file_unique_origin", false, locked);
