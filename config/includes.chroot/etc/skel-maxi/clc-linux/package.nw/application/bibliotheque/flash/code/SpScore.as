﻿package code
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.display.GradientType;
	import flash.geom.Matrix;
	
	
	public class SpScore extends Sprite
	{
		public var tfScore:CalcTextField;
		public var largeur:Number;
		public var padding:Number;
		
		
		public function SpScore()
		{
			tfScore=new CalcTextField();
			tfScore.multiline=false;
			tfScore.wordWrap=false;
			tfScore.textFormat.size=12;
			tfScore.textFormat.ajouteGras();
			tfScore.defaultTextFormat=tfScore.textFormat;
			addChild(tfScore);
			x=20;
			y=425;
		}
		
		private function dessinerFond():void
		{
			this.graphics.clear();
			var couleurTrait=0x0099CC;
			var couleurFond=0x0099CC;
						
			var gradientBoxMatrix:Matrix = new Matrix();
			gradientBoxMatrix.createGradientBox(tfScore.width+(padding*2), tfScore.height+(padding*2), Math.PI/2, 0, 0);
			this.graphics.lineStyle(3, couleurTrait, 1);
			this.graphics.beginGradientFill(GradientType.LINEAR, [couleurFond, couleurFond], [1,1], [0,255], gradientBoxMatrix);
			this.graphics.drawRoundRect(0, 0, tfScore.width+(padding*2), tfScore.height+(padding*2),10);
			
		}
	}
}