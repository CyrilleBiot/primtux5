var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.grenouille = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aNombres,champReponse,aLabel;

// Référencer les ressources de l'exercice (textes, image, son)
exo.oRessources = { 
    txt : "grenouille/textes/grenouille_fr.json",
    illustration : "grenouille/images/illustration.png",
    grenouille : "grenouille/images/grenouille.png",
    herbe : "grenouille/images/herbe.png",
    rocher : "grenouille/images/rocher.png",
    panneauG : "grenouille/images/panneau-g.png",
    panneauD : "grenouille/images/panneau-d.png"
};

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:3,
        totalEssai:1,
        tempsExo:0,
        temps_question:0,
        dX:"5;0,5;10",
        n_depart:"10;10;27"
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    var departs=exo.options.n_depart.split(";");
    departs.forEach(function(element,index){
        departs[index]=util.strToNum(element);
    });
    var deltas=exo.options.dX.split(";");
    deltas.forEach(function(element,index){
        deltas[index]=util.strToNum(element);
    });
    
    aNombres=[];
    for (i= 0;i<departs.length;i++)
    {
        var aTemp = [];
        aTemp[0] = departs[i] - deltas[i];
        aTemp[1] = departs[i];
        for (var j = 2 ; j < 9 ; j++ ) {
            aTemp[j] = departs[i]+deltas[i]*(j-1);
        }
        aNombres[i]=aTemp;
    }
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    exo.blocIllustration.html(disp.createImageSprite(exo,"illustration"));
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
		arrow:"disabled"
	});
    exo.btnValider.hide();
    //
    var q = exo.indiceQuestion;
    var delta = util.strToNum(exo.options.dX.split(";")[q]);
    var aValeur = aNombres[q];
    var vx;//deplacement horizontal
    vx  = delta > 0 ? 2 : -2;
    var vy = -10; // deplacement vertical
    var g = 0.5; // gravité
    //
    
    // la mare
    var cnt = disp.createSvgJsContainer(735,350);
    var mare = cnt.paper.rect(735,200).attr({fill:"#A3E3FE",stroke:'none'});
    mare.y(150);
    exo.blocAnimation.append(cnt);
    
    // les rochers
    for(var i = 0; i < 9; i++ ) {
        var rocher = disp.createImageSprite(exo,"rocher");
        exo.blocAnimation.append(rocher);
        rocher.css({left:10+80*i,top:220});
    }
    
    // les etiquettes
    aLabel = [];
    for(var i = 0; i < 9; i++ ) {
        var nombre = util.numToStr(aValeur[i]);
        var label = disp.createTextLabel(nombre);
        exo.blocAnimation.append(label);
        if (delta > 0) {
            label.css({left:40+(80*i)-label.width()/2,top:245});
        } else {
            label.css({left:680-(80*i)-label.width()/2,top:245});
        }
        if (i>1 && i<7) {
            label.hide();
        }
        aLabel[i]=label;
    }
    
    // les herbes
    for(var i = 0; i < 7; i++ ) {
        var herbe = disp.createImageSprite(exo,"herbe");
        exo.blocAnimation.append(herbe);
        herbe.css({left:10+100*i,top:90});
    }
    
    // le panneau
    var panneau;
    if (delta > 0) {
        panneau = disp.createImageSprite(exo,"panneauD");
    } else {
        panneau = disp.createImageSprite(exo,"panneauG");
    }
    exo.blocAnimation.append(panneau);
    panneau.css({left:300,top:60});
    
    // la grenouille
    var guernoule = disp.createImageSprite(exo,"grenouille");
    console.log(guernoule.width());
    var gTop = 175;
    if (delta > 0) {
        guernoule.css({left:80+20,top:gTop});
    } else {
        guernoule.css({left:7*80+20,top:gTop});
    }
    exo.blocAnimation.append(guernoule);
    var nRocher = 2;//le rocher sur lequel se trouve la grenouille
    
    // le champ reponse
    champReponse = disp.createTextField(exo,6);
    exo.blocAnimation.append(champReponse);
    champReponse.css({
        left:367.5-champReponse.width()/2,
        top:285
    });
    champReponse.focus();
    champReponse.on("keydown",function(e){
        if (e.which == 13) {
            gererValider(e);
        }
    });

    // la consigne
    var labelConsigne = disp.createTextLabel("Compte de "+delta+" en "+delta+" à partir de "+aValeur[0]+".");
    labelConsigne.css({fontSize:18,fontWeight:"bold"});
    exo.blocAnimation.append(labelConsigne);
    labelConsigne.css({left:20,top:20});
    
    // comportement de la touche "Valider" du clavier virtuel.
    var toucheValider = exo.keyboard.baseContainer.find('.key.large');
    toucheValider.data("valeur","exo");// permet de redefinir le comportement de la touche "Valider"
    toucheValider.on("touchstart", gererValider);
    
    
    
    
    //
    function bougerGrenouilleJuste(){
        if (guernoule.position().top <= gTop) {
            requestAnimationFrame(bougerGrenouilleJuste);
            vy += g;
            guernoule.css({left:'+='+vx,top:'+='+vy});
        } else {
            if(delta>0){
                guernoule.css({left:20+(nRocher-1)*80,top:gTop});
            } else {
                guernoule.css({left:20+(9-nRocher)*80,top:gTop});
            }
            
            vy = -10;
            aLabel[nRocher-1].show();
        }
    }
    
    function bougerGrenouilleFaux(){
        guernoule
            .transition({y:-80,rotate:360},500)
            .transition({rotate:-180},250)
            .transition({y:0},500)
            .transition({rotate:0,delay:1000},250);
    }
    
    function gererValider(e){
        e.preventDefault();
        repEleve = util.strToNum(champReponse.val());
        if (repEleve == aValeur[nRocher]) {
            bougerGrenouilleJuste();
            champReponse.val("");
            nRocher++;
            if (nRocher == 7) {
                champReponse.hide();
                toucheValider.off("touchstart", gererValider);
                //on redonne à la touche valider son comportement par défaut
                toucheValider.data("valeur","valider");
                exo.avancer();
            }
        } else if (repEleve === "") {
            //code
        } else {
            bougerGrenouilleFaux();
        }
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    return "juste";
};

// Correction (peut rester vide)
exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_question",
        texte:exo.txt.opt2
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:200,
        taille:20,
        nom:"dX",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:200,
        taille:20,
        nom:"n_depart",
        texte:exo.txt.opt4
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));