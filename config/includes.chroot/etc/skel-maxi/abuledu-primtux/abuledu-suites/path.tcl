package require Img
global iwish progaide basedir Home Home_data baseHome abuledu prof plateforme aideurl
set abuledu 0
set prof 0
set aideurl /usr/share/doc/abuledu-suites


set basedir [file dir $argv0]
cd $basedir
if {$env(HOME) == "c:\\"} {
set Home $basedir
} else {
set Home [file join $env(HOME) leterrier suites]
}

set baseHome $Home

set Home_data [file join $Home data]

switch $tcl_platform(platform) {
    unix {
	set progaide runbrowser
	set iwish wish
	}
    windows {
	set progaide shellexec.exe
	set iwish wish
	}
	}


proc initapp {plateforme} {
global basedir Home baseHome
lappend auto_path $basedir
cd $basedir


if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file copy -force [file join reglages] [file join $Home] 
	file copy -force [file join data] [file join $Home]

}

switch $plateforme {
    unix {
	if {![file exists [file join  $baseHome log]]} {
	file mkdir [file join  $baseHome log]
	}

    }
    windows {
	if {![file exists [file join suites log]]} {
	file mkdir [file join suites log]
	}
	
    	}
}

}

proc setwindowsusername {} {
    global user LogHome
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text [mc {Quel est ton nom?}] -background grey
    pack .utilisateur.frame.labobj -side top 

    listbox .utilisateur.frame.listsce -yscrollcommand ".utilisateur.frame.scrollpage set" -width 15 -height 10
    scrollbar .utilisateur.frame.scrollpage -command ".utilisateur.frame.listsce yview" -width 7
    pack .utilisateur.frame.listsce .utilisateur.frame.scrollpage -side left -fill y -expand 1 -pady 10
    bind .utilisateur.frame.listsce <ButtonRelease-1> "verifnom %x %y"
    foreach i [lsort [glob [file join $LogHome *.log]]] {
    .utilisateur.frame.listsce insert end [string map {.log \040} [file tail $i]]
    }

    button .utilisateur.frame.ok -background gray75 -text [mc {Ok}] -command "interface; destroy .utilisateur"
    pack .utilisateur.frame.ok -side top -pady 70 -padx 10
}

proc verifnom {x y} {
    global env user LogHome filuser baseHome

set ind [.utilisateur.frame.listsce index @$x,$y]
set nom [string trim [.utilisateur.frame.listsce get $ind]]
    if {$nom !=""} {
	set env(USER) $nom
	set filuser $nom.conf

	if {! [file exists [file join $baseHome reglages $filuser]]} {
	set filuser aller.conf
	}

	set user [file join $LogHome $nom.log]
	wm title . "[mc {Le bon chemin}] - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"

    }
}

proc initlog {plateforme ident} {
global LogHome user Home baseHome
switch $plateforme {
    unix {
	set LogHome [file join $baseHome log]

    }
    windows {
	set LogHome [file join suites log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome suites.log]
     }
}

proc inithome {} {
global baseHome basedir Home Home_data
variable repertconf
set f [open [file join $baseHome reglages repert.conf] "r"]
set repertconf [gets $f]
close $f

switch $repertconf {
0 {set Home $baseHome}
1 {set Home $basedir }
}
set Home_data [file join $Home data]
}

proc changehome {} {
    global Home basedir baseHome Home_data
    variable repertconf
 

    set f [open [file join $baseHome reglages repert.conf] "w"]
    puts $f $repertconf
    close $f
    switch $repertconf {
	0 {set Home $baseHome}
	1 {set Home $basedir}
    }
set Home_data [file join $Home data]
}

proc setlang {lang} {
global env plateforme baseHome
set env(LANG) $lang
set f [open [file join $baseHome reglages lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
tk_messageBox -message [mc {Les changements prendront effets au redemarrage de l'application.}] -type ok -title "Suites"
}
