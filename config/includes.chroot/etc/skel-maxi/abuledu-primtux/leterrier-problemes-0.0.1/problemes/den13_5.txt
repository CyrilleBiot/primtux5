set etapes 1
set niveaux {0 1}
::1
set niveau 1
set ope {{1 3} {1 7} {1 9}}
set interope {{1 10 1} {1 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set operations {{0+0}}
set volatil 0
set enonce "La monnaie.\nChoisis les pi�ces et les billets pour avoir [expr $ope1*100 + $ope2*10 + $ope3] euros."
set cible {{4 3 {} source0 100} {4 3 {} source1 10} {4 3 {} source2 1}}
set intervalcible 50
set taillerect 50
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 700
set source {euro100.gif euro10.gif euro1.gif}
set orient 0
set labelcible {{Billets de 100} {Billets de 10} {Pi�ces de 1}}
set quadri 0
set ensembles [list [expr $ope1] [expr $ope2] [expr $ope3]]
set reponse [list [list {1 3 5} [list {Il faut} [expr $ope1] {billet(s) de 100} [expr $ope2] {billet(s) de 10} [expr $ope3] {pi�ce(s) de 1}]]]
set dessin 0
set canvash 340
set c1height 60
set opnonautorise {}
::
