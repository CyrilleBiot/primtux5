set etapes 1
set niveaux {0 1}
::1
set niveau 1
set ope {{1 9} {1 9}}
set interope {{1 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
set operations {{0+0}}
set editenon "($ope1 enveloppes de 10 images\net $ope2 enveloppes de 5 images sont représentées.)"
set enonce "Les images.\nPim a [expr $ope1*10 + $ope2*5] images, qu'il range dans des enveloppes de 5 et de 10.\nCombien peut-il en remplir de chaque?"
set cible {{4 3 {} source0 10} {6 10 {} source1 5}}
set intervalcible 60
set taillerect 40
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 700
set source {enveloppe10.gif enveloppe5.gif}
set orient 0
set labelcible {{Enveloppes de 10} {Enveloppes de 5}}
set quadri 0
set ensembles [list [expr $ope1] [expr $ope2]]
set reponse [list [list {1 3} [list {Il peut avoir} [expr $ope1] {grande(s) enveloppe(s) de 10 et} [expr $ope2] {petite(s) enveloppe(s) de 5.}]]]
set dessin ""
set canvash 300
set c1height 160
set opnonautorise {}
::
