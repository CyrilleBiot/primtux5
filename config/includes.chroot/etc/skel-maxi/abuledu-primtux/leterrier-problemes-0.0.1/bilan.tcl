############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#bilan.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global plateforme LogHome file
source path.tcl
source fonts.tcl

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
initlog $plateforme $ident

set c .frame.c
set bgn #ffff80
set bgl #ff80c0

source fonts.tcl

source msg.tcl


. configure -background black -width 640 -height 480
wm geometry . +0+0
set file [file join $LogHome $argv]

wm title . Bilan\040[lindex [split [lindex [split $file /] end] .] 0]

menu .menu -tearoff 0
# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add command -label [mc {Effacer la fiche}] -command "effacefiche"
.menu.fichier add command -label [mc {Imprimer la fiche}] -command "imprimefiche"
.menu.fichier add command -label [mc {Fermer}] -command "exit"

. configure -menu .menu

text .text -yscrollcommand ".scroll set" -setgrid true -width 49 -height 20 -wrap word -background black -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

.text tag configure green -foreground green -font $sysFont(l)
.text tag configure red -foreground red
.text tag configure yellow -foreground yellow
.text tag configure normal -foreground black
.text tag configure white -foreground white -font $sysFont(l)


if {[catch { set f [open [file join $file] "r" ] }] } {
 
      set answer [tk_messageBox -message [mc {Erreur de fichier}] -type ok -icon info]
      exit
}

set date "[clock format [clock seconds] -format "%d/%m/%Y"]"
set header "<head></head>"
if {$plateforme == "unix"} { set header "<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head>"}
if {$plateforme == "windows"} { set header "<head><meta http-equiv='Content-Type' content='text/html; charset=ISO 8859-1'></head>"}
set g [open [file join $LogHome tmp.htm] "w" ] 
puts $g  $header
puts $g  "<html><body>"
puts $g  "<p align = center><B>[mc {Logiciel Probl�mes Cycle 2}] - Fiche de [string map {.log \040} [file tail $user]] - $date</B></p>"

       while {![eof $f]} {
         set listeval [gets $f]
		if {[lindex $listeval 0] == ""} {break}
         .text insert end [lindex $listeval 3]\n white
	   .text insert end "[lindex $listeval 0] - Niveau : [lindex $listeval 1]\n" red
	   puts $g  "<p><font color=black>[lindex $listeval 0] - Niveau : [lindex $listeval 1] - [lindex $listeval 3]</font></p>"
	   foreach item [lindex $listeval 2] {
	   .text insert end $item\n green
	   puts $g  "<p><font color=blue>$item</font></p>"
	   }
         #.text insert end "[lindex $listeval 2] - [mc {Score}] : [lindex $listeval 3] % \n" white
	    #puts $g  "<p>[lindex $listeval 2] - [mc {Score}] : [lindex $listeval 3] %</p>"
	    puts $g  "<p><font color=black><hr></font></p>"                                  
          }
	   .text insert end \n
            
 
close $f
puts $g  "</body></html>"
close $g



proc imprimefiche {} {
global progaide LogHome
set fichier [string map {" " %20} [file join [pwd] $LogHome tmp.htm]]
exec $progaide file:///$fichier &
}

proc effacefiche {} {
global file LogHome
set response [tk_messageBox -message [mc {Voulez-vous vraiment effacer la fiche ?}] -type yesno ]
	if {$response == "yes"} {
	set f [open [file join $file] "w" ]
	close $f
	.text delete 0.0 end
	set g [open [file join $LogHome tmp.htm] "w" ]
	close $g 
}
}









