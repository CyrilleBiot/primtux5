Le secret de ma�tre Cornille (13).
Pas un sac, pas un grain de bl� ; pas la moindre farine sur les murs ni sur les toiles d'araign�e... On ne sentait pas m�me cette bonne odeur chaude de froment �cras� qui embaume dans les moulins... L'arbre de couche �tait couvert de poussi�re, et le grand chat maigre dormait dessus. 
La pi�ce du bas avait le m�me air de mis�re et d'abandon : un mauvais lit, quelques guenilles, un morceau de pain sur une marche d'escalier, et puis, dans un coin, trois ou quatre sacs crev�s d'o� coulaient des gravats et de la terre blanche.
Alphonse Daudet.