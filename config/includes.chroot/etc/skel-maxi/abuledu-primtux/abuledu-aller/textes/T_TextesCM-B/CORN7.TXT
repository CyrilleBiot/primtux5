Pas un sac, pas un grain de bl�; pas la moindre farine sur les murs ni sur les toiles d'araign�e... On ne sentait pas m�me cette bonne odeur chaude de froment �cras� qui embaume dans les moulins... L'arbre de couche �tait couvert de poussi�re, et le grand chat maigre dormait dessus. 
La pi�ce du bas avait le m�me air de mis�re et d'abandon: un mauvais lit, quelques guenilles, un morceau de pain sur une marche d'escalier, et puis, dans un coin, trois ou quatre sacs crev�s d'o� coulaient des gravats et de la terre blanche. 
C'�tait l� le secret de ma�tre Cornille! C'�tait ce pl�tras qu'il promenait le soir par les routes, pour sauver l'honneur du moulin et faire croire qu'on y faisait de la farine... 
Pauvre moulin ! Pauvre Cornille ! Depuis longtemps les minotiers leur avaient enlev� leur demi�re pratique. Les ailes viraient toujours, mais la meule tournait � vide. 
Les enfants revinrent tout en larmes me conter ce qu'ils avaient vu. J'eus le coeur crev� de les entendre... Sans perdre une minute, je courus chez les voisins, je leur dis la chose en deux mots, et nous conv�nmes qu'il fallait, sur l'heure, porter au moulin de Cornille tout ce qu'il y avait de froment dans les maisons.,. Sit�t dit, sit�t fait. Tout le 
village se met en route, et nous arrivons l�-haut avec une procession d'�nes charg�s de bl�, du vrai bl�, celui-l� ! 
(Extrait de: Le secret de ma�tre Cornille d'Alphonse Daudet)

