Le grand sac d'or (2).
Gobette s'impatientait. Elle se mit � tourner autour d'Antoine comme une toupie.
- Voyons, Antoine, insista la poule, tu pourrais avoir un immense jardin avec toutes les fleurs, tous les l�gumes, et tous les arbres que tu voudrais. Te rends-tu compte ? Un grand sac d'or !
- Laisse-moi tranquille, Gobette. Je me moque du grand sac d'or. Et mon vieux noisetier est encore plus important que le monsieur important.