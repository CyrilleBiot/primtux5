############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : $$$
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#reglages.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

#source msg.tcl
global sysFont Home Homeconf exo strtext listexo repert filuser allerconf categorie repertoire
catch {destroy .w1}
toplevel .w1
set basedir [file dir $argv0]
cd $basedir

set exo none
wm title .w1 [mc {Reglages}]
.w1 configure -background Steelblue2
set font $sysFont(l)
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set allerconf [gets $f]
close $f

frame .w1.frametop1 -width 132 -background Steelblue2
grid .w1.frametop1 -row 0 -column 0
label .w1.frametop1.labeltop1 -text [mc {Choix de l'exercice}] -background grey95 -width 25
grid .w1.frametop1.labeltop1 -column 0 -row 0
frame .w1.frametop2 -width 360
grid .w1.frametop2 -row 0 -column 1
label .w1.frametop2.labeltop2 -text [mc {Parametrages}] -background Steelblue2
grid .w1.frametop2.labeltop2 -column 0 -row 0

frame .w1.frameleft -height 300 -width 180 
grid .w1.frameleft -column 0 -row 1

set i 0
    foreach menu {Closure Reconstitution {Phrases melangees} {Mots melanges} {Texte a corriger} {Phrases sans espaces} {Phrases incompletes} {} {} {Flash} {Rapido} Ponctuation} {
    label .w1.frameleft.l$i -text [mc $menu] -width 29
    place .w1.frameleft.l$i -x 0 -y [expr 20*$i]
    bind .w1.frameleft.l$i <Any-Enter> "pushEnter $i"
    bind .w1.frameleft.l$i <Any-Leave> "pushLeave $i"
    bind .w1.frameleft.l$i <ButtonRelease-1> "select $i"
    incr i
    }
frame .w1.frame1 -background Steelblue2 -height 300 -width 360
grid .w1.frame1 -column 1 -row 1

###########################################################
proc select {i} {
global sysFont exo
    for {set k 0} {$k < 12} {incr k 1} {
    .w1.frameleft.l$k configure -background grey95
    }
.w1.frameleft.l$i configure -background Steelblue2
set exo $i
interfac $i
}
##############################################################

proc pushEnter {i} {
.w1.frameleft.l$i configure -cursor hand2
}

##########################################################
proc pushLeave {i} {
.w1.frameleft.l$i configure -cursor left_ptr
}

##########################################################"""

proc chargedonn�es {exo} {
global sysFont listexo Home baseHome allerconf
set aller [lindex $allerconf $exo]

    switch $exo {
    0 {
            .w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 2] == 0} {
            .w1.frame1.sel5 deselect
            }
            if {[lindex $aller 1] == 0} {
            .w1.frame1.sel4 deselect
            }
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
            if {[lindex $aller 5] == 1} {
            .w1.frame1.sel7 select
            }
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }

        }

    1 {
            .w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 2] == 0} {
            .w1.frame1.sel5 deselect
            }
            if {[lindex $aller 1] == 0} {
            .w1.frame1.sel4 deselect
            }
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
            if {[lindex $aller 5] == 1} {
            .w1.frame1.sel7 select
            }
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }

        }

    2 {
            .w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 5] == 1} {
            .w1.frame1.sel7 select
            }
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
        }

    3 {
            .w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
            if {[lindex $aller 5] == 1} {
            .w1.frame1.sel7 select
            }
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }


        }

    4 {
            .w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
            if {[lindex $aller 5] == 1} {
            .w1.frame1.sel7 select
            }
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }

        }

    5 {
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
            #if {[lindex $aller 5] == 1} {
            #.w1.frame1.sel7 select
            #}
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }

        }

    6 {
            .w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }

        }
      7 {

        }

      8 {

        }
	9 {
	.w1.frame1.sel[lindex $aller 0] select
            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }

        }

	10 {
	.w1.frame1.sel[lindex $aller 0] select           
	}

      11 {
            if {[lindex $aller 6] == 1} {
            .w1.frame1.sel8 select
            }
	
        }
      12 {
            .w1.frame1.sel[lindex $aller 0] select
	            if {[lindex $aller 4] == 0} {
            .w1.frame1.sel6 deselect
            }

        }

    }

}

########################################################""




proc interfac {exo} {
global sysFont listexo

catch {destroy .w1.frame1}
frame .w1.frame1 -background Steelblue2 -height 300 -width 360
grid .w1.frame1 -column 1 -row 1
bind .w1.frame1 <Destroy> "enregistre $exo"
                        
switch $exo {
    0 {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel5 -x 40 -y 180
            .w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel7 -text [mc {Lecture du texte}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel7 -x 40 -y 200
		.w1.frame1.sel7 deselect
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des mots � trouver}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 220
		.w1.frame1.sel8 deselect
            }
    1 {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel5 -text [mc {La liste d'aide est variable}] -variable sel5 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel5 -x 40 -y 180
            .w1.frame1.sel5 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel4 -x 40 -y 160
            .w1.frame1.sel4 select
            checkbutton .w1.frame1.sel7 -text [mc {Lecture du texte}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel7 -x 40 -y 200
		.w1.frame1.sel7 deselect
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des mots � trouver}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 220
		.w1.frame1.sel8 deselect
            }
    2 {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            #checkbutton .w1.frame1.sel4 -text [mc {Les champs de texte sont de longueur fixe}] -variable sel4 -relief flat -background Steelblue2 -activebackground grey
            #place .w1.frame1.sel4 -x 40 -y 160
            #.w1.frame1.sel4 select
            checkbutton .w1.frame1.sel7 -text [mc {Lecture des phrases}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel7 -x 40 -y 160
		.w1.frame1.sel7 deselect
            checkbutton .w1.frame1.sel8 -text [mc {Lecture du texte}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 180
		.w1.frame1.sel8 deselect

            }

    3 {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel7 -text [mc {Lecture des mots}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel7 -x 40 -y 120
		.w1.frame1.sel7 deselect
            checkbutton .w1.frame1.sel8 -text [mc {Lecture de la phrase}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 180
		.w1.frame1.sel8 deselect

            }
    4 {
            label .w1.frame1.menu1 -text [mc {Reponse :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Les marques de textes sont visibles.}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel7 -text [mc {Lecture des mots}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel7 -x 40 -y 120
		.w1.frame1.sel7 deselect
            checkbutton .w1.frame1.sel8 -text [mc {Lecture du texte}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 140
		.w1.frame1.sel8 deselect

            }
    5 {
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            #checkbutton .w1.frame1.sel7 -text [mc {Lecture des mots}] -variable sel7 -relief flat -background Steelblue2 -activebackground grey
            #place .w1.frame1.sel7 -x 40 -y 120
		#.w1.frame1.sel7 deselect
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des phrases}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 140
		.w1.frame1.sel8 deselect

            }

    6 {
            label .w1.frame1.menu1 -text [mc {L'aide doit apparaitre :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Des le debut}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Apres le premier essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Apres le deuxieme essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            checkbutton .w1.frame1.sel8 -text [mc {Lecture des phrases}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 120
		.w1.frame1.sel8 deselect

            }
    7  {
            }

    8  {
	}
      9 {
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select
            label .w1.frame1.menu1 -text [mc {Vitesse :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Rapide}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {Moyenne}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Lente}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select

            }
       10 {
            label .w1.frame1.menu1 -text [mc {Temps :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {10 s}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {20 s}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {30 s}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select

            }
    11 {
            checkbutton .w1.frame1.sel8 -text [mc {Lecture du texte}] -variable sel8 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel8 -x 40 -y 40
		.w1.frame1.sel8 deselect

            }
12 {
            label .w1.frame1.menu1 -text [mc {Correction apr�s :}] -background Steelblue2
            place .w1.frame1.menu1 -x 40 -y 60
            radiobutton .w1.frame1.sel1 -text [mc {Le premier essai}] -variable sel1 -relief flat -value 1 -background Steelblue2
            place .w1.frame1.sel1 -x 180 -y 60
            radiobutton .w1.frame1.sel2 -text [mc {le deuxi�me essai}] -variable sel1 -relief flat -value 2 -background Steelblue2
            place .w1.frame1.sel2 -x 180 -y 80
            radiobutton .w1.frame1.sel3 -text [mc {Le troisi�me essai}] -variable sel1 -relief flat -value 3 -background Steelblue2
            place .w1.frame1.sel3 -x 180 -y 100
            .w1.frame1.sel3 select
            checkbutton .w1.frame1.sel6 -text [mc {Le texte est visible au debut}] -variable sel6 -relief flat -background Steelblue2 -activebackground grey
            place .w1.frame1.sel6 -x 40 -y 20
            .w1.frame1.sel6 select


}

    }
chargedonn�es $exo
}
    button .w1.frameleft.ok -text [mc {Ok}] -command "destroy .w1"
    place .w1.frameleft.ok -x 80 -y 270

bind .w1 <Destroy> "enregistre $exo"

############################################################################################

proc enregistre {exo} {
global sysFont categorie Home baseHome filuser categorie repertoire allerconf

if {$exo == "none" || $exo==7 || $exo==8} {
return}
    switch $exo {
    0 {
             variable sel1
             variable sel4
             variable sel5
             variable sel6
		 variable sel7
		 variable sel8
             lappend aller $sel1
             lappend aller $sel4
             lappend aller $sel5
             lappend aller $categorie
             lappend aller $sel6
             lappend aller $sel7
             lappend aller $sel8
            }

    1 {
             variable sel1
             variable sel4
             variable sel5
             variable sel6
		 variable sel7
		 variable sel8
             lappend aller $sel1
             lappend aller $sel4
             lappend aller $sel5
             lappend aller $categorie
             lappend aller $sel6
             lappend aller $sel7
             lappend aller $sel8
            }

    2      {
             variable sel1
             variable sel4
             variable sel6
		 variable sel7
		 variable sel8
             lappend aller $sel1
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller $sel6
             lappend aller $sel7
             lappend aller $sel8
            }

    3 {
             variable sel1
             variable sel6
		 variable sel7
		 variable sel8
             lappend aller $sel1
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller $sel6
             lappend aller $sel7
             lappend aller $sel8
            }

    4 {
             variable sel1
             variable sel7
		 variable sel8
             variable sel6
             lappend aller $sel1
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller $sel6
             lappend aller $sel7
             lappend aller $sel8
            }

    5 {
             variable sel6
             variable sel7
		 variable sel8
             lappend aller 0
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller $sel6
             lappend aller 0
             lappend aller $sel8
            }

    6 {
             variable sel1
             variable sel6
		 variable sel7
		 variable sel8
             lappend aller $sel1
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller $sel6
             lappend aller 0
             lappend aller $sel8
            }

    11 {
		 variable sel8
             lappend aller 0
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller 0
             lappend aller 0
             lappend aller $sel8
            }

        7 {
            }

       8 {
            }

	9 {
	variable sel1
	variable sel6
      lappend aller $sel1
      lappend aller 0
      lappend aller 0
      lappend aller $categorie
      lappend aller $sel6
      lappend aller 0
      lappend aller 0

	}

	10 {
	variable sel1
      lappend aller $sel1
      lappend aller 0
      lappend aller 0
      lappend aller $categorie
      lappend aller 0
      lappend aller 0
      lappend aller 0
	}
       12 {
             variable sel1
		 variable sel6
             lappend aller $sel1
             lappend aller 0
             lappend aller 0
             lappend aller $categorie
             lappend aller $sel6
             lappend aller 1
             lappend aller 1

            }

    }
set allerconf [lreplace $allerconf $exo $exo $aller]
set f [open [file join $baseHome reglages $filuser] "w"]
puts $f $categorie
puts $f $repertoire
puts $f $allerconf
close $f
}


proc changerep {} {
global categorie Home baseHome strtext listexo filuser
set types {    {{Fichiers aller}            {.alr}        }
			{{Fichiers texte}            {.txt}        }
		}

set categorie [tk_getOpenFile -filetypes $types -initialdir $Home -title [mc {Fichier :}] ]
if {$categorie ==""} {
set categorie "Au choix"
}
.w1.frame2.categorie configure -wraplength 180 -height 4 -text $categorie
#set ext .conf
set f [open [file join $baseHome reglages $filuser] "w"]
puts $f $categorie
close $f
set strtext ""
set listexo {}
setlistexo [file join $categorie]

interface
if {[string first "\$t tag add bisque1" $strtext]!=-1} {
.w1.frameleft.l7 configure -text [lindex [lindex $listexo 0] 1]
} else {
.w1.frameleft.l7 configure -text [mc {Exercice 1}]
}
if {[string first "\$t tag add bisque2" $strtext]!=-1} {
.w1.frameleft.l8 configure -text [lindex [lindex $listexo 1] 1]
} else {
.w1.frameleft.l8 configure -text [mc {Exercice 2}]
}
#if {[string first "\$t tag add bisque3" $strtext]!=-1} {
#.w1.frameleft.l9 configure -text [lindex [lindex $listexo 2] 1]
#} else {
#.w1.frameleft.l9 configure -text [mc {Exercice 3}]
#}
#if {[string first "\$t tag add bisque4" $strtext]!=-1} {
#.w1.frameleft.l10 configure -text [lindex [lindex $listexo 3] 1]
#} else {
#.w1.frameleft.l10 configure -text [mc {Exercice 4}]
#}


}
###################################################################################"""





