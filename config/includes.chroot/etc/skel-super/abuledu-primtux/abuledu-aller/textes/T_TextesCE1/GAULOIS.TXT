UN FESTIN DE GAULOIS. 

A table, pour le repas du soir. 
De grands �clats de voix s'�l�vent de la maison : c'est l'heure du repas du soir, le seul pour lequel les Gaulois se mettent � table. 
La viande est le plat principal. 
Sur la table, des plats fument. Des l�gumes, du chou, des oignons, un grand chaudron rempli de boudin et de morceaux de porc. Il y aussi des fromages, un plat de pommes cuites, et de grandes miches de pain blanc. Tout pr�s d'eux, les d�neurs ont des tonneaux remplis d'une sorte de bi�re, la cervoise. 
Les Gaulois mangent le plus souvent sans couverts, avec leurs doigts : ils connaissent la cuilller, mais, ni le couteau, ni la fourchette. 
Les Romains ont l'habitude de manger couch�s, mais ici, en Gaule, tous mangent assis sur des tabourets.
