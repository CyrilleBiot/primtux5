Limaille.

C'est ainsi que font les souliers 
Qui n'ont rien de particulier : 
Ils n'essaient pas de remplacer 
L'herbe qu'ils arrachent des pr�s.

C'est ainsi que font les papiers
Quand on les a mal employ�s : 
Ils s'abattent tels des corneilles
Mais au plus pr�s, dans des corbeilles.

C'est ainsi que font les nuages
Avant que de prendre de l'�ge :
Ils se d�font et se refont
Mais toujours ce qu'ils sont se fond.

C'est ainsi que font les lunettes
Pour que les choses soient plus nettes :
Elles font tant et m�me mieux
Pour que les d�sirent vos yeux.

C'est ainsi que font les bouquins
Bien rang�s, class�s par quelqu'un : 
Ils ont tendance � dispara�tre
Mais ailleurs que par la fen�tre.

C'est ainsi que font les pupitres
Qui ne sont pourtant pas des pitres :
Pour qu'on fasse penser aux enfants
Qu'ils sont de petits �l�phants.