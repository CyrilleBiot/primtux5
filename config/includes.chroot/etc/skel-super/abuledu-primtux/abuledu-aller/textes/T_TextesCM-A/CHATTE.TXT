Elle m'aime au point de comprendre ce que je dis, et de venir me caresser la bouche quand elle entend le son de ma voix. Elle aime aussi les livres comme un vieux savant, cette Fanchette, et me tourmente chaque soir apr�s le d�ner pour que je retire de leur rayon deux ou trois gros Larousse de papa, le vide qu'ils laissent forme une esp�ce de petite chambre carr�e o� Fanchette s'installe et se lave; je referme la vitre sur elle, et son ronron prisonnier vibre avec un bruit de tambour voil�, incessant. De temps en temps, je la regarde, alors elle me fait signe avec ses sourcils, qu'elle l�ve, comme une personne. Belle Fanchette, que tu es int�ressante et compr�hensive !




					Colette