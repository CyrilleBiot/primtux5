Ah! je savais bien que vous me reviendriez... Tous ces minotiers sont des voleurs. 
Nous voulions l'emporter en triomphe au village: 
-Non, non, mes enfants; il faut avant tout que j'aille donner � manger � mon moulin... Pensez donc! il y a si longtemps qu'il ne s'est rien mis sous la dent! 
Et nous avions tous des larmes dans les yeux de voir le pauvre vieux se d�mener de droite et de gauche, �ventrant les sacs, surveillant la meule, tandis que le grain s'�crasait et que la fine poussi�re de froment s'envolait au plafond. C'est une justice � nous rendre: � partir de ce jour-l�, jamais nous ne laiss�mes le vieux meunier manquer d'ouvrage. Puis, un matin, ma�tre Cornille mourrut, et les ailes de notre dernier moulin cess�rent de virer, pour toujours cette fois... Cornille mort, personne ne prit sa suite. Que voulez-vous, monsieur!... tout a une fin en ce monde, et il faut croire que le temps des moulins � vent �tait pass� comme celui des coches sur le Rh�ne, des parlements et des jaquettes � grandes fleurs. 
(Extrait de: Le secret de ma�tre Cornille d'Alphonse Daudet)

