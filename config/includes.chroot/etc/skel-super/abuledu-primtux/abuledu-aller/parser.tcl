#!/bin/sh
#parser.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************

proc parse2 {t} {
global plist2
    set plist ""
    set plist2 ""
    set cur 1.0
    while 1 {
      set cur [$t search -regexp -count len {[A-Z]} $cur end]
      if {$cur == ""} {
	    break
	}
      set tmp $cur
	#$t tag add d2 $cur "$cur + $len char"
      set cur [$t search -regexp -count length {[.!?\012]} $cur end]
	if {$cur == ""} {
	    break
	}
      set wordlist [regexp -inline -all -- {\S+} [$t get $tmp $cur]]
      set wordlist2 [regexp -inline -all -- {\S+} [$t get $tmp $cur+1c]]
	lappend plist2 $wordlist2
      lappend plist [regexp -inline -all -- {\S+} [string map {\{ \040 \} \040 \[ \040 \] \040 \" \040 \\" \040 , \040 ; \040 : \040 ( \040 ) \040 - \040} [$t get $tmp $cur]]]

    }
return $plist
}

proc parse {t} {
    set plist ""
    set cur 1.0
    while 1 {
      set cur [$t search -regexp -count len {[A-Z]} $cur end]
      if {$cur == ""} {
	    break
	}
      set tmp $cur
	#$t tag add d2 $cur "$cur + $len char"
      set cur [$t search -regexp -count length {[.!?\012]} $cur end]
	if {$cur == ""} {
	    break
	}
      set wordlist [regexp -inline -all -- {\S+} [$t get $tmp $cur]]
      lappend plist [regexp -inline -all -- {\S+} [string map {\{ \040 \} \040 \[ \040 \] \040 \" \040 \\" \040 ' \040 , \040 ; \040 : \040 ( \040 ) \040 - \040} [$t get $tmp $cur]]]

    }
return $plist
}

proc comptemots {plist} {
set compte 0
foreach phrase $plist {
set compte [expr $compte + [llength $phrase]]
} 
return $compte
}







