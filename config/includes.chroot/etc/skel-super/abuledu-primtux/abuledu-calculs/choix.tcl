#!/bin/sh
#choix.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier :
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *****************************************

#############Pr�paration du calcul : nom, classe, choix du sc�nario ou de la s�quence.

###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}

set basedir [pwd]
# [file dirname $argv0]
cd $basedir
source calculs.conf
source i18n
source path.tcl
set basedir [pwd]
global DOSSIER_EXOS1
##set  DOSSIER_EXOS1 [file join $Home data]
set  DOSSIER_EXOS1 [lindex $argv 0 ]
global sysFont
source msg.tcl
source fonts.tcl
global scenario
wm geometry . +0+0
wm title . "[mc {Choix du sc�nario}], pour expressions ou pluie"
## wm resizable . 0 0
. configure  -height 520 -width 640
set bgcolor sienna2
frame .frame -background  $bgcolor -height 520 -width 400
grid .frame -column 0 -row 0

label .frame.lab_prenom -text [mc {nom}] -background $bgcolor
place .frame.lab_prenom -x 130 -y 5
entry .frame.ent_nom -justify center
place .frame.ent_nom -x 80 -y 25
focus .frame.ent_nom
#set nom_elev $tcl_platform(user)
#[exec  id -u -n ]
#.frame.ent_nom insert end $nom_elev
#set nom_elev [.frame.ent_prenom get]
label .frame.lab_classe -text "[mc {ta_classe}] :" -background  $bgcolor
place .frame.lab_classe -x 120 -y 50
entry .frame.ent_classe -justify center
place .frame.ent_classe -x 80  -y 70
#set nom_classe [.frame.ent_classe get]

if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
if { $nom_classe == {} } {
set nom_classe classe
}
} else {
 set nom_elev eleve
 set nom_classe classe
}
.frame.ent_nom insert end $nom_elev
.frame.ent_classe insert end  $nom_classe
label .frame.duree_saut -text [mc {duree_max}] -background  $bgcolor
place .frame.duree_saut -x 225 -y 35
entry .frame.ent_duree
place .frame.ent_duree -x 290  -y 55  -width 35
.frame.ent_duree insert end 1000
set duree_saut [.frame.ent_duree get]
checkbutton .frame.unique -variable choix_unique -onvalue m  -text [mc {choix_pluie}] -background  $bgcolor -anchor e
place .frame.unique -x 15 -y 100

checkbutton .frame.hasard -variable flag_has -onvalue h  -text [mc {liste_hasard}] -background  $bgcolor -anchor e
place .frame.hasard -x 15 -y 130
checkbutton .frame.permut -variable flag_perm -onvalue p  -text [mc {permut_plus_fois}] -background  $bgcolor -anchor e
place .frame.permut -x 15 -y 150



 

if { $tcl_platform(platform) == "windows" } {
listbox .frame.list1 -background  $bgcolor -height 18 -width 40 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 80 -y 220
place .frame.scroll1 -x 310 -y 220 -height 290 } else {
listbox .frame.list1 -background  $bgcolor -height 15 -width 40 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 80 -y 220
place .frame.scroll1 -x 345 -y 220 -height 260
}

label .frame.menu3 -text [mc {double_clic_choix_sce_seq}]  -background  $bgcolor 
place .frame.menu3 -x 50 -y 185
label .frame.scenario -text [mc {sce_seq}] -background   $bgcolor
place .frame.scenario -x 125 -y 200
bind .frame.list1 <Double-ButtonRelease-1> { calc }
#pour lister les sc�narios et les s�quences d�j� construits.

proc peuplelist1 {}  {
 global DOSSIER_EXOS  basedir choix_lieu DOSSIER_EXOS1  env    Home
  cd  $DOSSIER_EXOS1
set ext .sce ; set ext1 .seq
    .frame.list1 delete 0 end
    catch {foreach i [lsort [glob [file join *$ext]]] {
            .frame.list1 insert end $i ;#"[file rootname $i]
        }
    }
catch {foreach i [lsort [glob [file join *$ext1]]] {
        .frame.list1 insert end $i ;#"[file rootname $i]
    }
}
}
peuplelist1

proc peuplelist2 {} {
global basedir DOSSIER_EXOS1
cd $DOSSIER_EXOS1
    set ext .sce ; set ext1 .seq
    .frame.list1 delete 0 end
    catch {foreach i [lsort [glob [file join *$ext]]] {
            .frame.list1 insert end $i ;#"[file rootname $i]
        }
    }
catch {foreach i [lsort [glob [file join *$ext1]]] {
        .frame.list1 insert end $i ;#"[file rootname $i]
    }
}
cd $basedir
}


#pour lancer le ou les claculs##############

proc calculs {} {
return [.frame.list1 get active]
}

proc  calc {} {
global basedir DOSSIER_EXOS1 choix_lieu   iwish   Home   DOSSIER_EXOS choix_unique flag_has flag_perm
set fich [calculs]
   if  {[file extension $fich] ==".sce" } {
    set liste_sce [list $fich]
} else  {
    cd $DOSSIER_EXOS1
    set f [open [file join $fich] "r"  ]
    set liste_sce [gets $f]
}
##pour d�tecter le dernier exo si s�quence
set len_liste [llength $liste_sce] ; set compt 1
foreach exo  $liste_sce {
    if {[file extension [lindex $liste_sce 0]] != ".sce" } {
        set exo "${exo}.sce"
    }
cd  $DOSSIER_EXOS1
#cd "/usr/share/abuledu/applications/abuledu-calculenpluie"
if { $choix_unique != "m" } { 
if {  $compt !=  $len_liste  }  {
incr compt
exec  $iwish  [file join $basedir pluie_un.tcl] [file join $DOSSIER_EXOS1 $exo ]  [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree get]  $DOSSIER_EXOS1 $flag_has $flag_perm
set rep [tk_messageBox  -message "[mc {exo_suivant}]" -type yesno -icon question  -default no -title "Exercice suivant?" ]
 if {  $rep  == "no"  }  { break }
		} else  {
exec  $iwish  [file join $basedir pluie_un.tcl] [file join $DOSSIER_EXOS1 $exo ]  [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree get]  $DOSSIER_EXOS1 $flag_has $flag_perm &  ; destroy .
		}
}  else  {
if {  $compt !=  $len_liste  }  {
incr compt
exec  $iwish  [file join $basedir pluie.tcl ] [file join $DOSSIER_EXOS1 $exo ] [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree  get] $DOSSIER_EXOS1 $flag_has $flag_perm 
set rep [tk_messageBox  -message "[mc {exo_suivant}]" -type yesno -icon question  -default no -title "Exercice suivant?" ]
 if {  $rep  == "no"  }  { break }
} else {
exec  $iwish  [file join $basedir pluie.tcl ] [file join $DOSSIER_EXOS1 $exo ] [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree  get] $DOSSIER_EXOS1 $flag_has $flag_perm & ; destroy .
}
}
} ;#"return [.frame.list1 get active]
}




