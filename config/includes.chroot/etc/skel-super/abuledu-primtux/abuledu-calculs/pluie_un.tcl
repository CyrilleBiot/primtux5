#!/bin/sh
#pluie.tcl   ESSAI DE RECONSTRUCTION D'UN Logiciel IPT (auteur ?)
#\
exec wish "$0" ${1+"$@"}
###########################################################################
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
 source calculs.conf
##source i18n

#bind . <F1> "showaide { }"
set basedir [pwd] ; cd $basedir
global sysFont
source msg.tcl
source fonts.tcl
 source path.tcl
 set basedir [pwd]
set bgcolor orange1
set bgl #ff80c0
wm geometry . +0+0
. configure -background $bgcolor
#wm resizable . 0 0
wm  title . "[mc {cal_un_un}] -Sous GPL"
set date [clock format [clock seconds] -format "%H%M%D"]
frame .frame -width 1024 -height 800 -background $bgcolor
#bframe pour les calculs (bframe.res)
frame .bframe -background $bgcolor -relief raised -bd 3 -height 10
##pack .bframe -side bottom -fill both
label .bframe.lab_consigne -background $bgcolor -text [mc {mess_calc}] -anchor n
##grid .bframe.lab_consigne -padx 20 -column 1 -row 1
button .bframe.but_calcul -text [mc {lancer_calc}] \
-command ".bframe.but_calcul configure -state disable ; calcul"
##grid .bframe.but_calcul -column 3 -row 1 -padx 100
##grid .bframe.but_calcul  .bframe.sol  -column 3 -row 1 -padx 5 
entry .bframe.res -width 20  -justify center -textvariable rep  -font {Arial 15}
##grid .bframe.res -column 2 -row 1 ; 
frame .mframe -background $bgcolor -relief raised -bd 3



frame .hframe -background $bgcolor
grid .hframe -row 0 -column 0 -sticky news
grid  .bframe -row 2 -column 0 -sticky news
grid .mframe -row 1  -column 0 -sticky news
### .mframe .bframe.bframe.lab_consigne  
grid  .bframe.lab_consigne -row 0 -column 0 -sticky w
grid .bframe.res -row 0 -column 1  -padx 10
grid .bframe.but_calcul -row 0 -column 2  -padx 10 -sticky e
##frame .mframe -background $bgcolor -relief raised -bd 3
##pack .mframe -side bottom -fill both 
#entry .mframe.ent_nom -width 20
#grid .mframe.ent_nom -column 1 -row 1 -sticky e
#label .mframe.lab_nom  -text "est   ton nom " -background #ffff80  -anchor e
#grid .mframe.lab_nom -column 2 -row 1
set nom_elev  [lindex $argv 1 ] ;#init nom �l�ve et nom clase
set nom_classe [lindex $argv 2 ]
initlog  $tcl_platform(platform) $nom_elev
label .mframe.lab_nom  -text "[mc {bonjour}] $nom_elev" \
         -background $bgcolor -anchor n -font {arial 10}
  #AC# column 1 par 5
###  grid .mframe.lab_nom -column 0 -row 0

set fich [lindex $argv 0 ]
set  DOSSIER_EXOS1 [lindex $argv 4]
cd $DOSSIER_EXOS1
set f [open [file join $fich] "r"  ]
set listdata [gets $f] ;#init de la liste des couples.
#set fich $f
#"set fich [file tail $f]

close $f
cd $basedir
proc vieux_fichier {expr} {
set e1 [regsub -all  {\/}  $expr ":" var]
set e2 [regsub -all  {\*} $var "x" var1 ]
set e3 [regsub -all  {\.} $var1 "," var2 ]
return $var2
}
set oper [ vieux_fichier [lindex  $listdata 0] ]
##est ce x ou :
if { $oper == "x" } { set oper_c "*" } 	elseif { $oper == ":" } {set oper_c "/" } else {set oper_c $oper }

set n_lig [lindex  $listdata 2]
#set n_col [lindex  $listdata 3]
set n_col 1 
if { $n_lig > 17 } {
set n_lig 17
}
proc traite_expr { expr} {
if { [string last + $expr] != -1  || [string last - $expr] != -1  \
 || [string last x $expr] != -1  || [string last : $expr] != -1 } {
return "\(${expr}\)"
} else {
return $expr
}
}
proc traite_expr1 {expr} {
 if { [string last "\(" $expr ] == -1  } {
 return $expr
    } else {
	return [expr $expr]
	}
}
proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {,} $var1 "\." var2 ]
return $var2
}
proc traite_expr { expr} {
if { [string last + $expr] != -1  || [string last - $expr] != -1  \
 || [string last x $expr] != -1  || [string last : $expr] != -1 } {
return "\(${expr}\)"
} else {
return $expr
}
}



#hframe pour la tomb�e des expressions.  par grid
##frame .hframe -background #808080
##pack .hframe -side bottom -fill both
set j 1
for {set i 1} {$i <= $n_lig} {incr i 1} {
           label .hframe.lab_bilan$i$j -text "___________"  -background $bgcolor -font {Arial 20}
        grid .hframe.lab_bilan$i$j -column $j -row $i 
    }

label .mframe.rep -text "                               " -font {Arial 10} -justify center -background $bgcolor
label .mframe.rep1 -text "                               " -font {Arial 10} -background $bgcolor
### ; place .hframe.rep  -x 410 -y 200
grid .mframe.rep -column 2 -row 0 -padx 5  ;# essai pour les bad calculs
grid .mframe.rep1 -column 1 -row 0 -padx 5
message .hframe.rep_bon -text "                   \n                 " -background $bgcolor 
grid .hframe.rep_bon -column 3 -row 1 -padx 20 
message .hframe.rep_bon1 -text "                   \n                 " -background $bgcolor 
grid .hframe.rep_bon1 -column 0 -row 1 -padx 20
set icone_b	[image create photo -file [file join  $basedir images bien.png]   ]
##label .hframe.bien -image $icone_b -background $bgcolor
set icone_m	[image create photo -file [file join  $basedir images mal.png]   ]
set fin [ image create photo fin	-file [file join  images "fin.gif"]]	
button .hframe.exit   -image $fin -command " exit"	
#grid .hframe.exit -column [expr $n_col + 1 ] -row $n_lig


##label .hframe.mal -image $icone_m -background $bgcolor
##labels inutiles (destroy label
#"set listdata {+ {1 2 5 3 14 5 7 8 4 5 1 5 14 5 23 5 4 15 8 9 0 3 4 5  78 1}}
#init des couples de nombre en jeu, de l'op�ration, des couples au d�part
#des bons calculs et des mauvais
##liste � m�langer selon les choix !!!flag_has flag_perm
set flag_has [lindex $argv 5 ]  ; set flag_perm [lindex $argv 6 ]
if { $oper == ":" || $oper == "-" } { set flag_perm "non" }
##.bframe.res insert  0 "$flag_has  et $flag_perm"
proc list_ind_has {list1 } {
 if { [llength $list1 ] == 1 } {return $list1 } else {
set i [expr int([expr [llength $list1 ]*(rand())]) ]
return [linsert  [list_ind_has [lreplace $list1 $i $i]] 0 [lindex $list1 $i] ]
}
}

proc list_ind {list} {
set l []
  if { [llength $list] == 2 } {return {0} } else {
		for {set i 0} { $i <= [ expr [ llength $list] - 2] } {incr i 2} {
		set l [linsert $l end $i]
}
}
return $l
}



proc fais_has {list} {
set l ""
foreach el [list_ind_has [list_ind $list] ] {
set l [concat $l [lindex $list $el]  [lindex $list [ expr $el + 1]] ]
}
 return $l
}
##int([expr $max - $min]*(rand()))
proc pair {} {
set n 1000
set tir [expr int([expr 1000*(rand())]) ]
if { [expr $tir % 2] == 0} {return 1} else {
		return 0 }
}

##.bframe.res insert  0	"$flag_has $flag_perm flag_h p [pair] [pair][pair] [pair]"


if { $flag_has == "h" } {
set l1 [list 1 2 3 4 5 6  ]
##.bframe.res insert  end "[list_ind $l1] et [expr int([expr [llength $l1 ]*(rand())]) ]hh [list_ind_has $l1 ]"
##.bframe.res insert  end [fais_has "4 5 6 8 9 12"]
		set listcouples  [fais_has [vieux_fichier [lindex $listdata 1] ] ]} else {
##	.bframe.res insert  0	"nnh"
set listcouples [vieux_fichier [lindex $listdata 1] ]}

if { $listcouples  == {} } {
 tk_messageBox -message  [mc {mess_list3}] -title "[mc {Message mauvaise liste}]" -icon error -type ok
exit  }
##set oper [lindex $listdata 0]
set ndepart [expr int($n_col / 2) ]
#set ndepart 1; #< nbre colonnes -1  
##set listcouples [lindex $listdata 1]
set uu [llength $listcouples]
set nbrecouples [expr $uu /2]
##premier couple 
 set nx [traite_expr [lindex [lrange $listcouples 0 1] 0] ]
    set ny [ traite_expr [lindex [lrange $listcouples 0 1] 1]]
    set ii [expr ($i /2) + 1]
	switch $flag_perm {
				"p" {
				if { [pair] == 0  } {
				
				.hframe.lab_bilan11  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
				} else {
				.hframe.lab_bilan11  configure -text "$ny$oper$nx"  -background $bgcolor -font {Arial 20}
				}
				}
				default {
				.hframe.lab_bilan11  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
				}
					}
	
##.hframe.lab_bilan11  configure -text "${nx}${oper}${ny}"  -background $bgcolor -font {Arial 20}

if {[lindex $argv 4] == {} } {
set approx 5
   } else {
set approx [lindex $argv 4]
}
set listcouples [lrange $listcouples 2 end]
set nbrecouplesrestant [expr $nbrecouples - 1 ]
set indexcol 2
#attention � ne pas d�passer $depart"
set score 0 ; set echec 0 ; set reussite 0 ; set mrep 0 ; set bonneliste {} ; set echecliste {} ;
set tempo [lindex $argv 3] ; set drapeau 1 ; set drapeau_bon 0 ; set bon_lig 0   ; set troptard {}
if { ![regexp {^[0-9]+$} $tempo ] } {
set tempo 1000  } 
proc memorise {} {
global input
set input [.bframe.res get]
.bframe.res delete 0 end
}
proc compte_car { car chaine } {
set occur  0
for { set i 0 } {$i <= [expr  [string length $chaine ] - 1] } {incr i } {
if { [string compare [string index  $chaine $i ] $car ] == 0 } {
incr occur
}
}
return $occur
}

############Pour analyser les calculs######################################
##########################################################################"
proc solution { liste_rep op b_lis t_lis e_lis  nom  fich  date} {
global  env      tcl_platform w2
catch {destroy .top2 }
set w2 [toplevel .top2]
wm geometry $w2 +350+0
wm title $w2 [mc {bil_bon_calc}]
##raise $w2
###grab ${w2}
text ${w2}.text -yscrollcommand "${w2}.scroll set" -setgrid true -width 60 -height 15 \
 -wrap word -background black -font   {Helvetica  12}
scrollbar ${w2}.scroll -command "${w2}.text yview"
pack ${w2}.scroll -side right -fill y
pack ${w2}.text -expand yes -fill both
${w2}.text tag configure green -foreground green
${w2}.text tag configure green1 -foreground green -underline yes
${w2}.text tag configure red -foreground red
${w2}.text tag configure yellow -foreground yellow
${w2}.text tag configure normal -foreground black
${w2}.text tag configure white -foreground white -underline yes
${w2}.text tag configure white2 -foreground white
${w2}.text tag configure white1 -foreground white     -font  {helvetica 16}
 ${w2}.text tag configure  blue  -foreground  blue
proc marge {  n } {
global w2 ; set chaine  " "
for {set i 1 } {$i < $n} { incr i} {
set chaine [append chaine " "]
}
${w2}.text insert end  $chaine
}
###pour une liste r�ponse
proc presentation_un { rep n } {
global w2
##le premier est le calcul � faire
##les autres sauf dernier sont les calculs ou expressions faux
foreach el [lrange $rep 1 end-1 ] {
if { [regexp {^[0-9]+\,?[0-9]*$} $el ] } {
marge  2 ; ${w2}.text insert end  "MCal : $el " red
} else {
marge 2 ; ${w2}.text insert end  "MExp :  $el " red
}
}
##si le dernier est {} c'est trop tard
if { [lindex $rep end ] == {}  } {
	marge  1 ; ${w2}.text insert end   "[mc {trop_tard1} ] pour : [lindex $rep 0]"  yellow
	marge  1 ; ${w2}.text insert end  "\n[mc {un_bon_calc}] [lindex $rep 0]= [expr 1.0 * [change_oper [lindex $rep 0]]] " yellow
	}
##si le dernier nombre est �gal au premier c'est bon calcul sinon liste et on recommence
 if {[regexp {^[0-9]*\,?[0-9]+$} [lindex $rep end] ] && [expr [change_oper [lindex $rep end]]] == [expr [change_oper [lindex $rep 0 ]] ]  }  {
   marge  1  ; ${w2}.text insert end  "[mc {mess_bonne_rep}] : [lindex $rep end]"  white
   } else {
   if { [lindex $rep end ] != {}  } {
${w2}.text insert end "\n" ; marge  $n   ; ${w2}.text insert end  "B.expr ==>:" green
marge  1 ;   ${w2}.text insert end [lindex  [lindex $rep end] 0] green
  presentation_un  [lindex $rep end]   [ expr $n +  6]
   }
}
}
 ##pour toutes les listes reponses
proc  presentation { l_rep } {
global w2
if  { $l_rep == {}  } { return }
marge  1 ; ${w2}.text insert end  "[mc {ton_calc1}] " white
${w2}.text insert end  "[lindex  [lindex $l_rep 0] 0]" green1
presentation_un [lindex $l_rep 0]  6
${w2}.text insert end  "\n---------------------------------------------------------------------------------------\n" blue
presentation [ lrange $l_rep 1 end ]
}
${w2}.text insert end  "   Le $date, [mc {exercice}] : $fich\n"     red
${w2}.text insert end   "[mc {resum}] $nom  :\n"   white1
${w2}.text insert end "==============================================\n" white2
##{w2}.text insert end  "  $nom\n\n"
##marge 2 ; ${w2}.text insert end  "[mc {bon_calc}]\n"   white
presentation $liste_rep
if  {  $tcl_platform(platform)  == "unix"  }  {
set rep [tk_messageBox  -message "[mc {imprim}]" -type yesno -icon question  -default no -title Imprimer? ]
 if {  $rep  == "yes"  }  {
### set  t    [ ${w2}.text  get  0.0  end  ]
 exec lpr <<  [ ${w2}.text  get  0.1  end ]  
 ### pr -0 2  [ ${w2}.text  get  0.1  end  ] | lpr
 ###  $b_lis  $e_lis
 ### ${w2}.text  get  0 end  ]
 }
}

raise ${w2} .
}


bind .bframe.res <Return> "memorise"
bind .bframe.res <KP_Enter> "memorise"
set input {}
set drapeau_expr 0 ; set liste_expr {} ; set reussite_expr 0 ; set drapeau_expr_mcal 0 ; set  mauv_expr 0
set liste_expr_mauv {} ; set liste_rep {} ; set ind 1 ; set indice_ind 1 ; set liste_travail1 {} ; set liste_travail1 [linsert $liste_travail1 0 "$nx$oper$ny" ]
##############################################################################################
# programme principal avec usage pseudo recursif. R�le de after pas tr�s clair encore
# A l'arr�t : travail sauv� ds un fichier qui doit exister
##############################################################################################



proc calcul {} {
global fich tempo nom_elev n_col n_lig listcouples nbrecouples score echec avreu reussite troptard \
mrep nbrecouplesrestant indexcol input oper  bonneliste echecliste date drapeau bon_lig bon_col  liste_rep
global drapeau_bon drapeau_expr liste_expr reussite_expr drapeau_expr_mcal avreu_expr mauv_expr liste_expr_mauv ind indice_ind
global  liste_travail1  bgcolor icone_b icone_m flag_perm
##liste_travail2 liste_travail3 liste_travail4  liste_travail5  liste_travail6 liste_travail7 liste_travail8 liste_travail9  liste_travail10
focus -force .bframe
catch { destroy .hframe.bien ; destroy .hframe.mal}
#"update
        if  { ( $echec == $nbrecouples ) || \
( $reussite >= $nbrecouples ) || \
( [expr $echec + $reussite] >= $nbrecouples )} {
#focus -force .mframe.ent_bilan
label .mframe.lab_bilan  -text "$nom_elev, [mc {reussite}] $reussite calcul(s)." \
         -background $bgcolor  -anchor e
###    grid .mframe.lab_bilan -column 5 -row 1 -padx 15
###pour tester.bframe.res insert 0  $liste_rep
    sauver $date $nom_elev $fich $oper $liste_rep $troptard  $echecliste
  button .bframe.sol -text [mc {R�sum� du travail ?}] -foreground blue \
-command "solution {$liste_rep} $oper {$bonneliste} {$troptard} {$echecliste}  $nom_elev [file tail $fich] $date"
 grid .bframe.sol -column 3 -row 0
 button .bframe.bis -text [mc {Recommencer ? }] -foreground blue  -command "recommencer "
grid .bframe.bis -column 5 -row 0
.bframe.but_calcul configure -text [mc {quitter}] -state active -command { exit }
return 1
        }
          .hframe.lab_bilan${n_lig}1 configure -text "___________" -background  $bgcolor -font {Arial 20}
##set indexcol 1
        if  { ($nbrecouplesrestant != 0 ) && ($indexcol <= $n_col && $drapeau )  } {
            set acal [ lrange $listcouples 0 1 ]
            set listcouples [lrange $listcouples 2 end ]
            set nx [traite_expr [lindex $acal 0]] ; set ny [traite_expr [lindex $acal 1] ]
		if { ${bon_lig} != 1 && $drapeau_bon == 1 } {
			switch $flag_perm {
				"p" {
				if { [pair]  } {
				
				.hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
				} else {
				.hframe.lab_bilan1${indexcol}  configure -text "$ny$oper$nx"  -background $bgcolor -font {Arial 20}
				}
				}
				default {
				.hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
				}
					}
				} else {
				
				switch $flag_perm {
				"p" {
				if { [pair]  } {
				.hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
				} else {
				.hframe.lab_bilan1${indexcol}  configure -text "$ny$oper$nx"  -background $bgcolor -font {Arial 20}
				}
				}
				default {
				.hframe.lab_bilan1${indexcol}  configure -text "$nx$oper$ny"  -background $bgcolor -font {Arial 20}
				}
					}
				
				}
				
				
        

set indexcol 1
            set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
		set drapeau 0 ; set liste_travail1 [linsert $liste_travail1 0 "$nx$oper$ny" ] ; set ind 1
##drapeau pour ne passer qu'une fois par ici: au d�marrage.
		}

if { $drapeau_bon == 1} {
if { ${bon_lig} !=1 } {
.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "___________" -background $bgcolor -font {Arial 20}
#set drapeau_bon 0
#} else {
#.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "BON" -background $bgcolor -font {Arial 20}
set drapeau_bon 0
}
}
set avreu $reussite ; set avreu_expr $reussite_expr
 avancer
after $tempo  calcul
 }
### gestion des calculs de l'avanc�e...##############################
########################################################

proc avancer {} {
global rep1 n_lig n_col bon_lig bon_col drapeau_bon listcouples nbrecouplesrestant oper approx avreu  \
reussite echec mrep score bonneliste echecliste indexcol input   troptard drapeau_expr liste_expr 
global reussite_expr drapeau_expr_mcal avreu_expr  mauv_expr liste_expr_mauv ind  indice_ind  bgcolor
global liste_rep basedir icone_b icone_m flag_perm 
global  liste_travail1 liste_travail2 liste_travail3 liste_travail4  liste_travail5  liste_travail6 liste_travail7 liste_travail8 liste_travail9  liste_travail10
##global liste_travail${ind}
.hframe.rep_bon configure -text "                \n              " -font {Arial 10}
focus  .bframe.res ; set drapeau_bon 0 ; set drapeau_expr_mcal 0
set rep1 $input ;
##if  {![ regexp {(^[0-9]+$)} $rep1 ] == 1 || [ regexp {0+(^[8-9]+$)} $rep1 ]} {
##set input {} ; set rep1 {}
##}
##![regexp {^(\(?\d*[\+|\-|\*|\/]{0,1}\d\)?)+[\+|\-|\*|\/]*(\(?\d*[\+|\-|\*|\/]{0,1}\d\)?)+$} $rep1 tout prem deux]
if { ![ regexp {(^[0-9]+$)} $rep1 ] == 1  &&  \
![regexp {^(\(?([0-9]*\,?[0-9]*)*[\+|\-|x|\:]{0,1}([0-9]*\,?[0-9]*)*\)?)+[\+|\-|x|\:]*(\(?([0-9]*\,?[0-9]*)*[\+|\-|x|\:]{0,1}([0-9]*\,?[0-9]*)+\)?)+$} $rep1 tout prem deux]  } {
             set input {} ; set rep1 {}
}

if { [regexp {\/0+} $rep1 ] == 1 } {
set input {} ; set rep1 {}
}

if { [ regexp {\,{2,}} $rep1 ] == 1 || $rep1 == ","  } {
set input {} ; set rep1 {}
}
if { [regexp {[\+|\-|x|\:|\,]{2,}}  $rep1 ] == 1 || [regexp {[\+|\-|x|\:]{1}$} $rep1 ] == 1 } {
set input {} ; set rep1 {}
}
set nbre_ouv  [compte_car "(" $input ]
set nbre_ferm [compte_car ")" $input ]
if { $nbre_ouv != $nbre_ferm  } {
set input {} ; set rep1 {}
}
if { [regexp {[0-9]*\,[0-9]+\,[0-9]*}  $rep1 ] } {
set input {} ; set rep1 {}
}		 

# receuil de la r�ponse de l'�l�ve. La dur�e de la frappe d�pend de after 1500
# pour chaque case jusqu'� la ligne n_lig-1 faire :
for  {set col 1 } {$col <= $n_col } {incr col } {
    for  {set lig [expr $n_lig - 1]} {$lig > 0 } {incr lig -1} {
        set val [.hframe.lab_bilan${lig}${col} cget -text] ;# ce qu'il y a dans la case!
 if {[regexp {(\(?\d+[\+|\-|x|\:]{0,1}\d*\)?)+[\+|\-|x|\:]{0,1}(\(?\d+[\+|\-|x|\:]{0,1}\d?\)?)*} $val tout prem deux]  } {
                  set nx  [traite_expr $prem ]
            set ny  [ traite_expr $deux ] ; set inter [expr [expr 1.0 * [change_oper $val] ]]
if { $rep1 != ""} {
set rep2 [expr [ change_oper $rep1 ]] ; set rep1 $rep2
}
##[expr [change_oper $rep1 ] ]
             if  { $rep1 != ""  && $inter ==   $rep1   && [ regexp {(^[0-9]*\,?[0-9]*$)} $input ]  }  {
           set drapeau_bon 1
##set icone_b	[image create photo -file [file join  $basedir images bien_mental.gif]   ]
label .hframe.bien -image $icone_b -background $bgcolor
grid  .hframe.bien -row ${lig} -column 3 -rowspan 4        	   
		   
		   
            incr score ; incr reussite ; #" .mframe.bil insert end $reussite
 ##.bframe.res insert 0   [set liste_travail${ind} ]
set bonneliste [ linsert $bonneliste end [linsert $liste_expr 0 $val ] ]
set   liste_travail${ind}  [linsert [set  liste_travail${ind}]    end $input ]
if { $ind == $indice_ind }  {
     set liste_rep [linsert $liste_rep end [set liste_travail${ind}] ]
     ##.bframe.res insert end $liste_rep
     } else  {  
     set  liste_p  [set liste_travail${ind} ] ; set liste {} 
     for { set j [expr $ind -  1]} { $j >= $indice_ind } { incr  j -1  } {
      set liste [linsert [set liste_travail$j]  end $liste_p ]
      set liste_p $liste  
     }
    set liste_rep [linsert $liste_rep end $liste] 
    ## .bframe.res insert end $liste_rep
     }
set bon_lig $lig ; set bon_col $col ; set input {}
 if { $bon_lig == 1 } {
.hframe.lab_bilan2${col}  configure -text "[mc {bon_calcul}]" -background #ffffff -font {Arial 20}
clignote [mc {bon_calcul}] 2 ${col} 2
.hframe.lab_bilan2${col}  configure -text "___________" -background $bgcolor -font {Arial 20} -foreground black
} else {
.hframe.lab_bilan${lig}${col}  configure -text "[mc {bon_calcul}]" -background #ffffff -font {Arial 20}
clignote [mc {bon_calcul}] ${lig} ${col} 2
.hframe.lab_bilan${lig}${col}  configure -text "___________" -background $bgcolor -font {Arial 20} -foreground black
}
if  { ($nbrecouplesrestant != 0) && ([string compare .hframe.lab_bilan1${col} "___________" ] != 0 ) } {
    set acal [ lrange $listcouples 0 1 ]
    set listcouples [lrange $listcouples 2 end ]
    set nx1 [traite_expr [lindex $acal 0]] ; set ny1 [traite_expr  [lindex $acal 1] ]
    set nbrecouplesrestant [expr $nbrecouplesrestant - 1] ; set liste_expr {} ; set drapeau_expr 0
 ##incr ind
  set ind 1 ; set indice_ind $ind ; global liste_travail{ind} ;##liste de travail global suivante
         set liste_travail${ind} {}  ;##global liste_travail{ind} ;##liste de travail global suivante
         set liste_travail${ind}   [linsert [set liste_travail${ind} ] 0 "$nx1$oper$ny1" ] 
		 
		 switch $flag_perm {
				"p" {
				if { [pair] == 0  } {
				
				.hframe.lab_bilan1${col}  configure -text "$nx1$oper$ny1"  -background $bgcolor -font {Arial 20}
				} else {
				.hframe.lab_bilan1${col}  configure -text "$ny1$oper$nx1"  -background $bgcolor -font {Arial 20}
				}
				}
				default {
				.hframe.lab_bilan1${col} configure -text "$nx1$oper$ny1"  -background $bgcolor -font {Arial 20}
				}
					}
		 
		 
 ##      .hframe.lab_bilan1${col} configure -text "${nx1}${oper}${ny1}" -background $bgcolor -font {Arial 20}
}


## .bframe.res delete 0 end
#".hframe.lab_bilan${lig}${col}  configure -text "___ " -background #808080 -font {Arial 14}

set input {}
break ;##fin  bonne r�ponse chiffr�e
}  else  {
##bonne expression non chiffr�e cad expressiion
if  { $inter == $rep1 } {
label .hframe.bien -image $icone_b -background $bgcolor
grid  .hframe.bien -row ${lig} -column 3 -rowspan 4    

      set k [expr $lig + 1] 
        set liste_expr [linsert $liste_expr end $input]  
##nelle liste travail s'ouvre	
##premi�re bonne expression liste de travail s'ouvre
   if {  $drapeau_expr   == 0 } {
incr ind ;  global liste_travail${ind}  ; set liste_travail${ind} {}
  set  liste_travail${ind} [ linsert [set liste_travail${ind}] end $input ]
}  else  {
 ##set u "liste_travail"
incr ind ;  global liste_travail${ind}  ; set liste_travail${ind} {}
 set  liste_travail${ind} [ linsert [set liste_travail$ind ] end $input ] }
set liste_expr [linsert $liste_expr 0 $val]  
   set drapeau_expr 1
.hframe.lab_bilan${k}${col} configure -text "${input}" -background $bgcolor -font {Arial 20}
.hframe.lab_bilan${lig}${col}  configure -text "___________" -background $bgcolor -font {Arial 20}
clignote [mc {bonne_expr}] ${lig} ${col} 2
.hframe.lab_bilan${lig}${col}  configure -text "___________" -background $bgcolor -font {Arial 20} -foreground black
set input {} ; incr reussite_expr
break
}  else  {

#mauvaise r�ponse, on fait descendre  l'expression ou (bonne ou mauvaise expression) expression
##mauvaise expression mais expression
 if { ![ regexp {(^[0-9]*\.?[0-9]*$)} $input ]  && $input != {} } {
 label .hframe.mal -image $icone_m -background $bgcolor
grid  .hframe.mal -row ${lig} -column 3 -rowspan 4 
set liste_expr_mauv [linsert $liste_expr_mauv end $input] ; set  liste_travail${ind} [ linsert  [set liste_travail${ind} ] end $input ]
 set drapeau_expr 1 ; incr mauv_expr  ; set echecliste [ linsert $echecliste end  [linsert $liste_expr_mauv 0 $input ] ] ; set input {}
  }
set k [expr $lig + 1]
.hframe.lab_bilan${k}${col} configure -text "${val}" -background $bgcolor -font {Arial 20}
##${prem}${oper}${deux}
.hframe.lab_bilan${lig}${col}  configure -text "___________" -background $bgcolor -font {Arial 20}
if { $input != {} && ${drapeau_expr} == 0 } {
##.hframe.rep configure -text [mc {mauv_calcul}] -font {Arial 10} -foreground blue
##.bframe.res delete 0 end ;# on efface la mauvaise r�ponse de l'input !.
set echecliste [ linsert $echecliste end  $input]  ;  set  liste_travail${ind} [ linsert [set liste_travail${ind} ] end $input ]
### [list $prem $deux $input]]
label .hframe.mal -image $icone_m -background $bgcolor
grid  .hframe.mal -row ${lig} -column 3 -rowspan 4 
set input {} ; incr mrep
if { $drapeau_expr == 1 } { set drapeau_expr_mcal 1}
}
}  ;## fin mauvaise reponse ou expression bonne qui descend
	}  ;##fin bonne r�ponse chiffr�e
}  ;##fin for lignes
}  ;##fin for sur les colonnes
#il reste la derni�re ligne : les trop tard !
for  {set t 1} {$t <= $n_col} {incr t} {
    set val [.hframe.lab_bilan${n_lig}${t} cget -text]
 if {[regexp {(\(?\d*[+|\-|x|\:]{0,1}\d*\)?)[+|\-|x|\:](\(?\d*[+|\-|x|\:]{0,1}\d*\)?)} $val tout prem deux]  } {
            .hframe.lab_bilan${n_lig}${t} configure -text [mc {trop_tard1}] -background #ffffff -font {Arial 20}
                     incr echec ; set bonne_expr 0
	##				 label .hframe.mal -image $icone_m -background $bgcolor
	##		grid .hframe.mal -row [expr ${n_lig} - 2] -column 3 -rowspan 3
#.hframe.lab_bilan${n_lig}${t} configure -text [mc {Perdu}] -background #ffffff -font {Arial 20}
    set nx  $prem
    set ny  $deux 
 ## pour les trop tard   
    set  liste_travail${ind} [ linsert [set liste_travail$ind]  end {} ]
     if { $ind == $indice_ind }  {
     set liste_rep [linsert $liste_rep end [set liste_travail${ind}] ]
     }  else  {
     set  liste_p  [set liste_travail${ind} ] ; set liste {} 
     for { set j [expr $ind -  1]} { $j >= $indice_ind } { incr  j  -1 } {
      set liste [linsert  [set liste_travail$j ]  end $liste_p ]
      set liste_p $liste  
     }
    set liste_rep [linsert $liste_rep end $liste]
     }
 set  troptard [ linsert   $troptard end  $val]
if { $liste_expr != {}  && $drapeau_expr == 1 }  {
set bonneliste [linsert $bonneliste  end $liste_expr ]
set bonneliste [linsert $bonneliste  end {0+0} ]
}
set liste_expr {} ; set  liste_expr_mauv {} 
      if  { $nbrecouplesrestant != 0  } {
        set acal [ lrange $listcouples 0 1 ]
        set listcouples [lrange $listcouples 2 end ]
        set nx [traite_expr [lindex $acal 0] ] ; set ny [traite_expr [lindex $acal 1]]
	 set ind 1 ; set indice_ind $ind
## unset liste_travail1
     set liste_travail${ind}  {} ;  global liste_travail${ind}  ;##liste de travail global suivante 
    set liste_travail${ind}   [linsert [set liste_travail${ind} ]  0 "$nx$oper$ny" ]
        .hframe.lab_bilan1${t}  configure -text "$nx${oper}$ny"  -background $bgcolor -font {Arial 20}
        set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
    
    }
} ;##fin regexp

} ;##fin for
## si le nbe de reussites est inchang� c'est une mauvaise r�ponse sinon bonne r�ponse.

if  { $avreu == $reussite && $rep1 != "" && $drapeau_expr == 0 } {
	 set drapeau_expr 0 ; set  drapeau_expr_mcal 0
   .mframe.rep configure -text "[mc {mess_mauvais_rep}](${mrep})" -background $bgcolor -foreground green -font {Arial 10}
} else {

if { $avreu_expr == $reussite_expr && $rep1 != "" && $drapeau_expr != 0} {
set drapeau_expr 0
    .mframe.rep configure -text "[mc {mess_mauvais_rep_expr}](${mauv_expr})" -background $bgcolor -foreground green -foreground green -font {Arial 10}
} else {
 if { $rep1 !="" && [ expr $avreu_expr != $reussite_expr  || $avreu != $reussite ] } {
        .mframe.rep configure -text " [mc {mess_bonne_rep}]([expr ${reussite_expr} + ${reussite} ])" -background $bgcolor -foreground green -font {Arial 10} -justify center
      } else {
      .mframe.rep configure -text "                                "              
      }
}
}
return 1
}
}
proc clignote {texte lig col  nclic} { ; #after supprim� car semble pos� des pbs de // avec l'autre after
global  bgcolor
for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {Arial 20} -foreground green
update
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {Arial 20} -foreground red
update
}
}

# sauver les r�sultats
proc sauver {dat nom fic oper listereussite listetard  listechec} { ; # � s�curiser
global savingFile nom_classe basedir TRACEDIR LogHome  user choix_lieu approx DOSSIER_EXOS1 Home liste_rep
set TRACEDIR $LogHome
cd $TRACEDIR
#set types {
#    {"Cat�gories"		{.txt}	}
#}
#catch {set file [tk_getSaveFile -filetypes $types]}
#set f [open [file join $file] "a"]
#set travail  "date : $dat nom : $nom fichier : $fic $oper bon : $listereussite echec : $listechec  mauvais cal : $badcal"
set basefile [file tail $fic]
set savingFile [file join $basedir data  ${nom_classe}.bil ]
set ou [lrange [ split $DOSSIER_EXOS1 "/" ]  end-1 end]
if { [lsearch -exact  $ou "C:" ]  >= 0 } {
set ou [ lreplace $ou  [lsearch  -exact $ou "C:" ] [lsearch -exact $ou  "C:"  ] "C" ]
}
catch {set f [open [file join $savingFile] "a"]}
 set travail  "$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$liste_rep}&$listetard&$listechec&un"
puts $f $travail
close $f
##set ou [lrange [ split $DOSSIER_EXOS1 "/" ]  end-1 end]
catch {set flog  [open [file join $user] "a+"]}
set log "$dat&$nom&{[file tail ${fic}] de $ou}&$oper&$liste_rep&$listetard&$listechec&un"
puts  $flog  $log
close $flog
###exec   /usr/bin/leterrier_log  --message=$log   --logfile=$user
}
proc recommencer {} {
 global basedir DOSSIER_EXOS1 argv  iwish nom_elev nom_classe tempo flag_has flag_perm
 set exo [lindex $argv 0 ]  ; destroy .
 exec  $iwish  [file join $basedir pluie_un.tcl ] [file join $DOSSIER_EXOS1 $exo ] $nom_elev $nom_classe $tempo  $DOSSIER_EXOS1 $flag_has $flag_perm &
}









