 #!/bin/sh
#choix.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *****************************************

#############Pr�paration du calcul : nom, classe, choix du sc�nario ou de la s�quence.

###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}

set basedir [pwd]
# [file dirname $argv0]
cd $basedir
source calculs.conf
source path.tcl
set basedir [pwd]
set bgcolor orange
bind . <F1> "showaide {division}"
global sysFont
source msg.tcl
source fonts.tcl
global scenario
set  DOSSIER_EXOS1 [lindex $argv 0 ]
wm geometry . +0+0
wm title . "[mc {choix_sce}] pour calcul de quotients"
. configure  -height 480 -width 640 -background $bgcolor 
frame .frame -background $bgcolor  -height 480 -width 340
grid .frame -column 0 -row 0

label .frame.lab_prenom -text [mc {nom}] -background $bgcolor  
place .frame.lab_prenom -x 130 -y 5
entry .frame.ent_nom -justify center
place .frame.ent_nom -x 80 -y 25
focus .frame.ent_nom
#set nom_elev $tcl_platform(user)
#[exec  id -u -n ]
#.frame.ent_nom insert end $nom_elev
#set nom_elev [.frame.ent_prenom get]
label .frame.lab_classe -text [mc {classe}] -background $bgcolor 
place .frame.lab_classe -x 130 -y 50
entry .frame.ent_classe -justify center
place .frame.ent_classe -x 80  -y 70
#set nom_classe [.frame.ent_classe get]
##label .frame.t_reglage -text [mc {fich_syst_util}] -background $bgcolor 
##place .frame.t_reglage  -x 30 -y 95

if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
} else {
 set nom_elev eleve
 set nom_classe classe
}
.frame.ent_nom insert end $nom_elev
.frame.ent_classe insert end  $nom_classe

if { $tcl_platform(platform) == "unix" } {
listbox .frame.list1 -background #c0c0c0 -height 20 -width 40 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 30 -y 145
place .frame.scroll1 -x 295 -y 145 -height 320 } else {
listbox .frame.list1 -background #c0c0c0 -height 20 -width 40 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 30 -y 145
place .frame.scroll1 -x 260 -y 145 -height 320

}
label .frame.menu2 -text [mc {fich_scenarios}]  -background $bgcolor 
label .frame.menu3 -text [mc {double_clic_scena_div}]  -background $bgcolor 
place .frame.menu3 -x 45 -y 120
##label .frame.scenario -text [mc {sce_seq}] -background $bgcolor 
##place .frame.scenario -x 85 -y 135
bind .frame.list1 <Double-ButtonRelease-1> { calc }
#pour lister les sc�narios et les s�quences d�j� construits.
proc dans { el liste } {
foreach el_gen $liste {
if { $el_gen == $el } {
return 1
}
}
return 0
}

 proc choix_scenaconf {} {
  global scena_div  DOSSIER_EXOS1  DOSSIER_EXOS choix_lieu   basedir Home
   set types {
    {"Cat�gories"		{.dconf}	}
}
   if {  $DOSSIER_EXOS1 == $basedir } {
   set scena_div "scenarios_div.dconf"
    return
}
   ##set scena_div   "essai.conf"
   ### [.frame1.fich-scena get] si vide...
  catch {set scena_div  [tk_getOpenFile -filetypes $types -initialdir $DOSSIER_EXOS1  -title "Choix du fichier de sc�narios"] }
}

proc peuplelist1 {} {
 global DOSSIER_EXOS_DIV   basedir choix_lieu   Home  DOSSIER_EXOS1    scena_div
   choix_scenaconf
##cd $DOSSIER_EXOS1
.frame.list1   delete 0 end
if {[catch {set f [open [file join $DOSSIER_EXOS1 $scena_div] "r"]}]} {
        bell
        return
   	 } else {
	set liste_noms {}
	while { [gets $f ligne] >=0 } {
 	if { [lindex [split $ligne ":"] 1] == "" } {
 	continue
 	}
 	set liste_noms [linsert $liste_noms 0 [lindex [split $ligne ":"] 0]]
	}
close $f
.frame.menu2 configure -text  "[mc {fich_scenarios}] [file tail $scena_div ]"
place .frame.menu2 -x 40  -y 100
set liste_noms [lsort $liste_noms ]
set liste_sans_doublons {}
foreach nom $liste_noms {
if { ![dans $nom $liste_sans_doublons] } {
set liste_sans_doublons [linsert $liste_sans_doublons 0 $nom ]
.frame.list1 insert end $nom
}
}
}
#cd $basedir
}
peuplelist1

proc calculs {} {
return [.frame.list1 get active]
}

proc  calc {} {
global basedir DOSSIER_EXOS_DIV   iwish DOSSIER_EXOS1 Home      scena_div
 set fich [calculs]
exec  $iwish  [file join $basedir  division.tcl ] $fich  [.frame.ent_nom  get] [.frame.ent_classe get]   ${DOSSIER_EXOS1}  $scena_div & ; destroy . 
}
#"return [.frame.list1 get active]

