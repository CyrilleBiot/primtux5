############################################################################
# Copyright (C) 2007 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editparcours.tcl,v 1.1.1.1 2004/04/16 11:45:45 erics Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
#source utils.tcl
#source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
#changehome


. configure -background white

menu .menu
# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add command -label "[mc {Ouvrir}] (Crtl-o)" -command "ouvre"
.menu.fichier add command -label "[mc {Enregistrer}] (Crtl-w)" -command "enregistre_sce"
.menu.fichier add separator
.menu.fichier add command -label "[mc {Quitter}] (Crtl-q)" -command "destroy ."
. configure -menu .menu



proc sauve {} {
global file Home
set types {    {{Fichiers txt}            {.txt}        }}
set ext .txt
catch {set file [tk_getSaveFile -filetypes $types -initialdir [file join $Home problemes] -initialfile $file]}
    if {$file != ""} {
        if  {[string match -nocase *.txt $file]==0} {
        set file $file$ext
        }
}
return $file
}

proc ouvre {} {
global file prob niveaux niveau ope sysFont Home
set editenon ""

set types {    {{Fichiers txt}            {.txt}        }}

set file [tk_getOpenFile -initialdir [file join $Home problemes] -filetypes $types]
if {$file != ""} {
wm title . "Editeur de Probl�mes : [file tail $file]"

set f [open [file join  $Home problemes [file tail $file]] "r"]
set tmp [gets $f]
set tmp [gets $f]
eval $tmp
	while {$tmp != "::1"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {eval $tmp}
	if {[string first enonce $tmp]!= -1} {break}
	}
close $f
##################################################################################"
catch {destroy .geneframe}
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left -anchor n

frame .geneframe.frame1 -background white -width 100 
grid .geneframe.frame1 -padx 10 -column 0 -row 0
##################################################
label .geneframe.frame1.enonce -bg white -text "$enonce\n$editenon" -wraplength 400
grid .geneframe.frame1.enonce -row 0 -column 0 -sticky news 
button .geneframe.frame1.but0 -text [mc {ok}] -command "fin" -activebackground white
grid .geneframe.frame1.but0 -column 0 -row 1 -padx 10 -pady 10

frame .geneframe.frame2 -background white -width 100 
grid .geneframe.frame2 -padx 10 -column 1 -row 0 
label .geneframe.frame2.a1 -bg white -text "Niveau : $niveau"
grid .geneframe.frame2.a1 -row 0 -column 0 -sticky news
listbox .geneframe.frame2.listsce -yscrollcommand ".geneframe.frame2.scrollpage set" -width 3 -height 6
scrollbar .geneframe.frame2.scrollpage -command ".geneframe.frame2.listsce yview"
grid .geneframe.frame2.listsce  -column 0 -row 1 -sticky nwes -pady 10
grid .geneframe.frame2.scrollpage -column 1 -row 1 -sticky nwes -pady 10
bind .geneframe.frame2.listsce <ButtonRelease-1> "changelistsce %x %y"
foreach item $niveaux {
.geneframe.frame2.listsce insert end $item
}


frame .geneframe.frame3 -background white -width 100 
grid .geneframe.frame3 -padx 10 -column 2 -row 0
label .geneframe.frame3.a1 -bg blue -foreground white -font $sysFont(s) -text "Valeur des op�randes entre :"
grid .geneframe.frame3.a1 -pady 5 -column 0 -row 0 -columnspan 3
set ind 0
set ordinl {premier deuxi�me troisi�me quatri�me}
set nombres ""
for {set k 0} {$k < [llength $ope]} {incr k} {
set indic [expr $k +1]
lappend ordin [lindex $ordinl $k]
lappend nombres [expr \$ope$indic]

}
foreach item $ope {
label .geneframe.frame3.a1$ind -bg white -font $sysFont(s) -text "Le [lindex $ordin $ind] nombre,[lindex $nombres $ind] ,doit �tre compris  entre :"
grid .geneframe.frame3.a1$ind -pady 5 -column 0 -row [expr $ind*2 +1] -columnspan 3

scale .geneframe.frame3.sc$ind -orient horizontal -length 220 -from [lindex [lindex $interope $ind] 0] -to [lindex [lindex $interope $ind] 1] -resolution [lindex [lindex $interope $ind] 2] -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
grid .geneframe.frame3.sc$ind -column 0 -row [expr $ind*2 +2]  -sticky "w e"
label .geneframe.frame3.aa1$ind -bg white -font $sysFont(s) -text "et"
grid .geneframe.frame3.aa1$ind -pady 5 -column 1 -row [expr $ind*2 +2]

scale .geneframe.frame3.sc$ind$ind -orient horizontal -length 220 -from [lindex [lindex $interope $ind] 0] -to [lindex [lindex $interope $ind] 1] -resolution [lindex [lindex $interope $ind] 2] -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s)
grid .geneframe.frame3.sc$ind$ind -column 2 -row [expr $ind*2 +2] -sticky "w e"
.geneframe.frame3.sc$ind set [lindex [lindex $ope $ind] 0]
.geneframe.frame3.sc$ind$ind set [expr [lindex [lindex $ope $ind] 0]+ [lindex [lindex $ope $ind] 1]*[lindex [lindex $interope $ind] 2]]
bind .geneframe.frame3.sc$ind <ButtonRelease-1> "changescalemin $ind"
bind .geneframe.frame3.sc$ind$ind <ButtonRelease-1> "changescalemax $ind"
incr ind
}
}
}

###################################################################
proc changescalemin {min} {
if {[.geneframe.frame3.sc$min get] > [.geneframe.frame3.sc$min$min get] } { 
.geneframe.frame3.sc$min$min set [.geneframe.frame3.sc$min get] 
}
}

proc changescalemax {max} {
if {[.geneframe.frame3.sc$max$max get] < [.geneframe.frame3.sc$max get] } { 
.geneframe.frame3.sc$max set [.geneframe.frame3.sc$max$max get] 
}

}

proc interface {} {
frame .geneframe -background white -width 400 -height 200 
pack .geneframe
}

###################################################################################"
proc enregistre_sce {} {
global file ope niveau Home
set lstmp ""
	for {set k 0} {$k < [llength $ope]} {incr k 1} {
	lappend lstmp "[.geneframe.frame3.sc$k get] [expr [.geneframe.frame3.sc$k$k get] - [.geneframe.frame3.sc$k get]]"
	}
set stniveau "set niveau $niveau"
set stope "set ope [list $lstmp]"

	if {$file ==""} {set file [sauve]}
	if {$file != ""} {
	wm title . "Editeur de Probl�mes : [file tail $file]"
	set ind 0
	set f [open [file join $Home problemes [file tail $file]] "r"]
		while {![eof $f]} {
		set tmp$ind [gets $f]
		incr ind
		}
	close $f

	set tmp3 $stniveau
	set tmp4 $stope

	set f [open [file join $Home problemes [file tail $file]] "w"]
		for {set k 0} {$k < $ind} {incr k} {
		puts $f [expr \$tmp$k]
		}
	close $f
	}
}

proc fin {} {
enregistre_sce
destroy .
}

proc changelistsce {x y} {
global niveau
set niveau [.geneframe.frame2.listsce get @$x,$y]
.geneframe.frame2.a1 configure -text "Niveau : $niveau"
}


global prob file ope niveau
wm title . "Editeur de Probl�mes : Nouveau"
set itempar ""
set file ""
set prob ""
set ope ""
set niveau 0

interface

