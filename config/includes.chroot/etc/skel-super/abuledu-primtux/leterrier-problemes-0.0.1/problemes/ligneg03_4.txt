set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 2
set ope {{5 10} {15 20}}
set interope {{1 15 1} {10 50 1}}
set scaleb {0 50 1}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set operations [list [list [expr $ope1 ]+[expr $ope2 - $ope1]] [list [expr $ope2 -  $ope1]+[expr $ope1]] [list [expr $ope2]-[expr $ope1]]]
set enonce "Le jeu de l'oie.\nPim avance son pion de $ope1 cases.\nIl arrive sur la case  $ope2.\nSur quelle case se trouvait le pion de Pim?"
set reponse [list [list {1} [list {Il �tait sur la case} [expr $ope2 - $ope1]]]]
set source {{0}}
set resultdessin [expr $ope2 - $ope1]
set dpt $ope2
set opnonautorise {0 1}
set canvash 140
set c1height 50
set placearriv none
::