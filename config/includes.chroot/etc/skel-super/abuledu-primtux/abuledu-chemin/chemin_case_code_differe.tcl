#!/bin/sh
# chemin_cases_code_differe.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : chemin_cases_code_differe.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: chemin_case_code_differe.tcl,v 1.19 2006/03/27 13:13:15 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier
#  @project    Le terrier
#  @copyright  Andr� Connes
#
#***********************************************************************
global sysFont glob categorie

set categorie "case_code_differe"


source chemin.conf
source msg.tcl
source fonts.tcl
source eval.tcl


  #
  # langue par defaut
  #
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]
  #
  # couleurs des cases
  #
  set f [open [file join $glob(home_reglages) couleurs.conf] "r"]
  gets $f vcouleurs
  close $f
  if { $vcouleurs == "Daltonien" } {
    set glob(celcolor) $glob(celDaltonien)
    set glob(doncolor) $glob(donDaltonien)
    set glob(errcolor) $glob(errDaltonien)
  } else {
    set glob(celcolor) $glob(celNonDaltonien)
    set glob(doncolor) $glob(donNonDaltonien)
    set glob(errcolor) $glob(errNonDaltonien)
  } 
  
wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.89)]x[expr int([winfo vrootheight .]*0.8)]+0+0
. configure -background $glob(bgcolor)
wm title . "[mc {Codage_differe}]"

################################################

# ##
# sauver trace-eleve et parcours
# ##

proc sauver_trace_parcours2 {} {
  global glob fb fc
  set glob(nberreurs) $glob(sauve_r�ponse)
  sauver_trace_parcours
  $fb.consigne configure -state disable
    
      after [expr $glob(attente) * 1000] {
	if { $glob(boucle) < $glob(bouclemax) } {
	  set glob(session_finie) non
	} else {
          set glob(session_finie) oui
	} 
    }
    vwait glob(session_finie)

    if { $glob(session_finie) == "non" } {
      open_and_code
    } else {
      .bframe.but_gauche configure -state normal
      if { $glob(nberreurs) <= 2 } {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
          -image [image create photo -file [file join sysdata sourire.png]]
      } else {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
          -image [image create photo -file [file join sysdata pleurer.png]]
      }
      # exit
    }
}

################################################

proc recommencer { } {
  global glob
  set glob(boucle) 0
  for {set i 1} {$i <= $glob(bouclemax)} {incr i 1} {
    .bframe.tete$i configure -image pneutre -width 80
  }
  .bframe.but_gauche configure -state disable
}                    

proc init_dir_exos {} {
  global glob
  catch {set f [open [file join $glob(home_reglages) dir_exos.conf] "r"]}
  set glob(dir_exos) [gets $f]
  close $f
  if { $glob(dir_exos) == "Commun" } {
    set glob(repert) [file join grilles]
  } else {
    set glob(repert) [file join $glob(home_chemin) grilles]
  }
}

proc affiche_analyse {t} {
  global glob fc fm
  incr glob(boucle)
  $fm.consigne configure -state disable
  if { $t == 0 } {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
           -image [image create photo -file [file join sysdata pbien.gif]]
        .bframe.tete$glob(boucle) configure -image pbien -width 80
        set img_analyse pbien.gif
  } elseif { $t == -1 } {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
           -image [image create photo -file [file join sysdata ppass.gif]]
        .bframe.tete$glob(boucle) configure -image ppass -width 80
        set img_analyse ppass.gif
  } elseif { $t == -2 } {
         $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
           -image [image create photo -file [file join sysdata pmal.gif]]
        .bframe.tete$glob(boucle) configure -image pmal -width 80
        set img_analyse pmal.gif
  } else {
        $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
           -image [image create photo -file [file join sysdata pmal.gif]]
        .bframe.tete$glob(boucle) configure -image pmal -width 80
        set img_analyse pmal.gif
  } ;# if
  $fc create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
        -image [image create photo -file [file join sysdata $img_analyse]]
}

proc verifier_vents {fc fvc} {
  global fb glob
  $fb.nord configure -state disable
  $fb.ouest configure -state disable
  $fb.gomme configure -state disable
  $fb.est configure -state disable
  $fb.sud configure -state disable
  $fb.pouce configure -state disable
  bind . <Up> ""
  bind . <Down> ""
  bind . <Left> ""
  bind . <Right> ""
  bind . <Delete> ""
  bind . <Return> ""
  #
  # construction de la liste correcte de vents
  #        f_vents = vents du fichier
  #
  set glob(f_vents) {}
  for { set k 1 } { [expr 2 * $k] < [llength $glob(trace)]} {incr k} {
    set yo [lindex $glob(trace) [expr 2*$k-2]]
    set xo [lindex $glob(trace) [expr 2*$k-1]]
    set yn [lindex $glob(trace) [expr 2*$k]]
    set xn [lindex $glob(trace) [expr 2*$k+1]]
    if { [expr $xn-$xo] == 1 } {
        set vent "est"
    }
    if { [expr $xn-$xo] == -1 } {
        set vent "ouest"
    }
    if { [expr $yn-$yo] == 1 } {
         set vent "sud"
    }
    if { [expr $yn-$yo] == -1 } {
        set vent "nord"
    }
    lappend glob(f_vents) $vent
  }
  #
  # kmin = longueur de la liste des vents la plus courte
  #
  if { [llength $glob(f_vents) ] < [llength $glob(vents)] } {
        set kmin [llength $glob(f_vents)]
  } else {
        set kmin [llength $glob(vents)]
  }
  #
  set glob(type_r�ponse) [mc Bravo]
  set glob(sauve_r�ponse) "0"
  for { set k 1 } {$k <= $kmin} {incr k} {
    set yk [lindex $glob(trace) [expr 2*$k]]
    set xk [lindex $glob(trace) [expr 2*$k+1]]
    if { [lindex $glob(vents) [expr $k - 1]] == [lindex $glob(f_vents) [expr $k - 1]] } {
      $fc create rect \
        [expr $glob(org) + $xk*$glob(wlen) +1] \
        [expr $glob(org) + $yk*$glob(hlen) +1] \
        [expr $glob(org) + ($xk+1)*$glob(wlen) +1] \
        [expr $glob(org) + ($yk+1)*$glob(hlen) +1] \
        -fill $glob(doncolor) -width 1 ;# -tag rect($i,$j)
      $fvc.glob(vent_choix_$k) configure -bg $glob(doncolor)
    } else {
      $fc create rect \
        [expr $glob(org) + $xk*$glob(wlen) +1] \
        [expr $glob(org) + $yk*$glob(hlen) +1] \
        [expr $glob(org) + ($xk+1)*$glob(wlen) +1] \
        [expr $glob(org) + ($yk+1)*$glob(hlen) +1] \
        -fill $glob(errcolor) -width 1 ;# -tag rect($i,$j)
      $fvc.glob(vent_choix_$k) configure -bg $glob(errcolor)
      set glob(type_r�ponse) "[mc n_erreur] $k"
      set glob(sauve_r�ponse) [expr $kmin - $k]
      break
    }
  }
  #
  # erreur ? Quel type ?
  #
  if {$glob(type_r�ponse) != [mc Bravo] } {
        affiche_analyse $k
  } else {
    if { [llength $glob(f_vents) ] == [llength $glob(vents)] } {
        set glob(type_r�ponse) [mc Bravo]
        affiche_analyse 0
    }
    if { [llength $glob(f_vents) ] > [llength $glob(vents)] } {
        set glob(type_r�ponse) [mc incomplet]
        set glob(sauve_r�ponse) [expr [expr [llength $glob(f_vents) ]] - [expr [llength $glob(vents)]]]
        affiche_analyse -1
    }
    if { [llength $glob(f_vents) ] < [llength $glob(vents)] } {
        set glob(type_r�ponse) [mc troplong]
        set glob(sauve_r�ponse) [expr [expr [llength $glob(vents) ]] - [expr [llength $glob(f_vents)]]]
        affiche_analyse -2
    }
  }
  #
  # sauvegarde des traces
  #
  sauver_trace_parcours2

} ;# verifier_vents

proc effacer_dernier_vent {fvc} {
  global glob
  set k [llength $glob(vents)]
  if { $k > 0 } {
        set glob(vents) [lreplace $glob(vents) [expr $k - 1] [expr $k - 1] ]
  $fvc.glob(vent_choix_$k) configure \
        -image [image create photo -file [file join sysdata fl_gomme.png]] \
        -borderwidth 2 -bg $glob(errcolor)
  grid $fvc.glob(vent_choix_$k) -row 0 -column $k
  }
}

proc lajouter {fvc vent} {
  global glob
  lappend glob(vents) $vent
  set k [llength $glob(vents)]
  if { [catch {label $fvc.glob(vent_choix_$k) \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg yellow} r ]  } {
    $fvc.glob(vent_choix_$k) configure \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg yellow
  }
  grid $fvc.glob(vent_choix_$k) -row 0 -column $k
}

##########################################################################
#
  proc open_and_code { } {
#
##########################################################################
  global fc fb fm fvc glob
  #
  # ouvrir le fichier des cases du chemin
  #

  # charger la liste des chemins du niveau disponibles
  set fnames [glob [file join $glob(repert) *$glob(boucle)_*]] 
  # choisir un chemin au hasard dans cette liste
  set fname [lindex $fnames [expr int(rand()*[llength $fnames])]]
  set glob(trace_fname) [file tail $fname]
  catch {set f [open $fname "r"]}
  catch {set glob(trace) [gets $f]}
  if {[catch {close $f} r]} {
        exit
  }
  # eventuellement renverser ce chemin
  set r [expr rand()]
  if { $r < 0.5 } {
    # renverser le chemin
    set tmp_liste {}
    for { set i 0 } { [expr 2*$i] < [llength $glob(trace)] } { incr i} {
      set tmp_liste [linsert $tmp_liste 0 [lindex $glob(trace) [expr 2*$i+1]]]
      set tmp_liste [linsert $tmp_liste 0 [lindex $glob(trace) [expr 2*$i]]]
    }  
    set glob(trace) $tmp_liste
  }
  #
  # on cree la grille (ATTENTION : i=ligne et j=colonne)
  #
  for {set i 0} {$i < $glob(nbrow)} {incr i 1} {
    for {set j 0} {$j < $glob(nbcol)} {incr j 1} {
      $fc create rect \
        [expr $glob(org) + $j*$glob(wlen) +1] \
        [expr $glob(org) + $i*$glob(hlen) +1] \
        [expr $glob(org) + ($j+1)*$glob(wlen) +1] \
        [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
        -fill $glob(celcolor) -width 1 ;# -tag rect($i,$j)
    }
  }
  #
  # afficher les cases du chemin
  #
  for { set n 1 } { [expr 2*$n] < [llength $glob(trace)] } { incr n } {
    set yn [lindex $glob(trace) [expr 2*$n]]
    set xn [lindex $glob(trace) [expr 2*$n+1]]
    $fc create rect \
        [expr $glob(org) + $xn*$glob(wlen) +1] \
        [expr $glob(org) + $yn*$glob(hlen) +1] \
        [expr $glob(org) + ($xn+1)*$glob(wlen) +1] \
        [expr $glob(org) + ($yn+1)*$glob(hlen) +1] \
        -fill yellow -width 1 ;# -tag rect($i,$j)
  }
  #
  # creation du frame pour les fleches choisies
  #
  catch {destroy .fframe}
  frame .fframe -background $glob(bgcolor) -width $glob(width)
  pack .fframe -side bottom -fill both -expand yes
  #
  # cr�er le canvas pour les messages et la suite...
  #
  set fm .fframe.m
  canvas $fm -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fm ;# -expand true
  label $fm.consigne \
        -text [mc {choisir_fleche}] -bg $glob(bgcolor)
  grid $fm.consigne -row 1 -column 0 -columnspan 20
  #
  # creer le canvas pour les fleches choisies
  #
  set fvc .fframe.vents_choisis
  canvas $fvc -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fvc ;# -expand true
  #
  # pb d'affichage (bande du bas trop haute) resolu maladroitement par astuce
  #
  label $fvc.vide \
        -image [image create photo -file [file join sysdata vide.png]] -bg $glob(bgcolor)
  grid $fvc.vide -row 0 -column 0
  #
  # creer le canvas pour les fleches a choisir
  #
  catch { destroy .frame.boussole }
  set fb .frame.boussole
  canvas $fb -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fb -expand true
  #
  # initialiser la liste des reponses (les vents choisis)
  #
  set glob(vents) {}
  #
  # placer les vents de direction en bas
  #
  set vent nord
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "lajouter $fvc $vent"
  grid $fb.$vent -row 1 -column 6
  set vent ouest
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "lajouter $fvc $vent"
  grid $fb.$vent -row 2 -column 5
  set vent gomme
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "effacer_dernier_vent $fvc"
  grid $fb.$vent -row 2 -column 6
  set vent est
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "lajouter $fvc $vent"
  grid $fb.$vent -row 2 -column 7
  set vent sud
  button $fb.$vent \
        -image [image create photo -file [file join sysdata fl_$vent.png]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "lajouter $fvc $vent"
  grid $fb.$vent -row 3 -column 6
  #
  # pour les messages
  #
  label $fb.consigne \
        -text [mc {puis}] -bg $glob(bgcolor)
  grid $fb.consigne -row 7 -column 6
  button $fb.pouce \
        -image [image create photo -file [file join sysdata ok1.gif]] \
        -borderwidth 2 -bg $glob(doncolor) \
        -command "verifier_vents $fc $fvc"
  grid $fb.pouce -row 9 -column 6
  #
  # les fleches du clavier
  #
  bind . <Up> "lajouter $fvc nord"
  bind . <Down> "lajouter $fvc sud"
  bind . <Left> "lajouter $fvc ouest"
  bind . <Right> "lajouter $fvc est"
  bind . <Delete> "effacer_dernier_vent $fvc"
  bind . <Return> "verifier_vents $fc $fvc"
  #
  # on marque la case de depart
  #
  set yn [lindex $glob(trace) 0]
  set xn [lindex $glob(trace) 1]
  $fc create rect [expr $glob(org) + $xn*$glob(wlen) +1] [expr $glob(org) + $yn*$glob(hlen) +1] \
        [expr $glob(org) + ($xn+1)*$glob(wlen) +1] [expr $glob(org) + ($yn+1)*$glob(hlen) +1] \
        -fill $glob(doncolor) -width 1 ;# -tag rect($i,$j)
  #
  # on passe la main a l'eleve
  #
  set glob(case_suivante) 1
  set glob(nberreurs) 0
  # top depart
  set glob(heure_debut) [clock seconds]

} ;# open_and_code

########################################################################
#                           programme principal                        #
########################################################################

bind . <Control-q> exit

  # Relire le nom de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    catch {set f [open [file join $glob(home_chemin) reglages trace_user] "r"]}
    gets $f glob(trace_user)
    close $f
  }
  
  init_dir_exos

  # creation du frame pour la grille et les fl�ches � choisir
  ###########################################################
  . configure -background $glob(bgcolor)
  # pour la grille et l'orientation
  frame .frame -background $glob(bgcolor) -height $glob(height) -width $glob(width)
  pack .frame -side top -fill both -expand yes

  # creation du canvas pour la grille
  ###################################

  set fc .frame.c
  canvas $fc -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fc -side left -expand true

  # ###########################################################
  # on cree une frame en bas en avant-derniere ligne avec
  #   le score affiche sous forme de tetes (bien passable mal)
  #   un bouton 'continuer'
  # ###########################################################

  frame .bframe -bg $glob(bgcolor)
  pack .bframe -side bottom -expand true

  image create photo pbien -file [file join sysdata pbien.gif] 
  image create photo ppass -file [file join sysdata ppass.gif]
  image create photo pmal -file [file join sysdata pmal.gif]
  image create photo pneutre -file [file join sysdata pneutre.gif]

  for {set i 1} {$i <= $glob(bouclemax)} {incr i 1} {
    label .bframe.tete$i -bg $glob(bgcolor) -width 4
    grid .bframe.tete$i -column [expr $i -1] -row 1 -sticky e
    .bframe.tete$i configure -image pneutre -width 80
  }

  button .bframe.but_gauche -image \
    [image create photo fgauche -file [file join sysdata fgauche.gif]] -command "recommencer; open_and_code"
  grid .bframe.but_gauche -column [expr $glob(bouclemax)+2] -row 1
  .bframe.but_gauche configure -state disable

  button .bframe.but_quitter -image \
    [image create photo fquitter \
    -file [file join sysdata quitter_minus.gif]] \
    -command exit
  grid .bframe.but_quitter  -column [expr $glob(bouclemax)+3] -row 1

open_and_code

