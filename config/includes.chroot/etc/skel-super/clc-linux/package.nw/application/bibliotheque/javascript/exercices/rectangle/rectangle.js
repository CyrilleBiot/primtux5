var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.rectangle = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var cnt, cntsvg, LeCercle, aNombre, divQuad, LeRectangle, rectCorrection, longueure, largeure,longueurq,largeurq, repEleve, soluce;

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    txt : "rectangle/textes/rectangle_fr.json",
    LeCercle : "rectangle/images/cercle.png",
    illustration : "rectangle/images/illustration.png"
}
// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:10,
        totalEssai:2,
        tempsExo:0,
        longueur:'3-10',
        largeur:'3-9'
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    aNombre=[];
    var aNombresA=[];
    var aNombresB=[];
    var param_a=exo.options.longueur;
    var param_b=exo.options.largeur;    
    if(param_a.indexOf("-")<0){
	//il s'agit d'une liste de 1 ou plusieurs nombres
	aNombresA=param_a.split(";");
    }else{
	//il s'agit d'un intervale
	var aTemp = [];
	aTemp=param_a.split("-");
	//
	var nMiniA=Number(aTemp[0]);
	var nMaxiA=Number(aTemp[1]);
	var nRangeA=nMaxiA-nMiniA;
	for(i=0;i<=nRangeA;i++){
	    aNombresA[i]=nMiniA+i;
        }
    }
    if(param_b.indexOf("-")<0){
	//il s'agit d'une liste de 1 ou plusieurs nombres
	aNombresB=param_b.split(";");
    }else{
	//il s'agit d'un intervale
	var aTemp = [];
	aTemp=param_b.split("-");
	//
	nMiniB=Number(aTemp[0]);
	nMaxiB=Number(aTemp[1]);
	var nRangeB=nMaxiB-nMiniB;
	for(i=0;i<=nRangeB;i++){
	    aNombresB[i]=nMiniB+i;
	}
    }
    //on genere tous les couples possibles 
    var aPossibles=[];
    for(i=0;i<aNombresA.length;i++){
	for(j=0;j<aNombresB.length;j++){
	    aPossibles.push([Number(aNombresA[i]),Number(aNombresB[j])]);
    	}
    }
    //on supprime les doublons sur le resultat de la multiplication
    var aCouples=[];
    aCouples[0]=aPossibles[0];
    for(i=1;i<aPossibles.length;i++){
	var existe_deja=false;
	    for (j=0;j<aCouples.length;j++){
		if(aPossibles[i][0]*aPossibles[i][1]==aCouples[j][1]*aCouples[j][0] ){
		    existe_deja=true;
		    break;
		}
	    }
	    if(!existe_deja){
		aCouples.push(aPossibles[i]);
	    }
    }
    //on Mélange le tableau
    aCouples.sort(function(){return Math.floor(Math.random()*3)-1});
    //on ne laisse pas sortir le zero
    var supprime_zero=false;
    for(i=0;i<aCouples.length;i++){
	if(aCouples[i][0]==0 || aCouples[i][1]==0 ){
	    aCouples.splice(i,1);
	    i--;
	}
    }	
    //on ajoute des couples si le nombre de couples obtenus est inférieur au nombre de questions
    var n=aCouples.length;
    var dif=exo.options.totalQuestion-aCouples.length;
    if(dif>0){
	for(i=n;i<exo.options.totalQuestion;i++){
	    aCouples.push(aCouples[i%n]);
	}
    }
    aNombre = aCouples;
}
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration)
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    exo.keyboard.config({
		numeric : "disabled ",
		float : "disabled ",
		delete : "disabled ",
		arrow : "disabled ",
		large : "disabled ",
		valider : "enabled "
    });
    //la consigne
    longueurq=aNombre[q][0];
    largeurq=aNombre[q][1];
    var aire=longueurq*largeurq;
    soluce = aire;
    etiquetteQuestion = disp.createTextLabel(exo.txt.consigne1+" "+aire+" "+exo.txt.consigne2);
    exo.blocAnimation.append(etiquetteQuestion);
    etiquetteQuestion.css({
        fontSize:18,
        width:300,
        left:450,
        top:50
    });
    //fond pour le drop
    var fond2 =  disp.createEmptySprite();
    fond2.css({
        width:530,
        height:322,
        left:100,
        top:20
    });
    exo.blocAnimation.append(fond2);
    //le quadriallage
    cnt = disp.createSvgJsContainer(360,360).css({left:40,top:20});
    cntsvg = cnt.paper.group();
    var fond = cnt.paper.rect(360,360).fill("#DEF2FF");
    aBalles=[];
    var pathTxt = '';
    divQuad=30;
    for (i=0;i<13;i++) {
        pathTxt+='M0,'+i*divQuad+'L'+12*divQuad+','+i*divQuad+'';
    }
    for (i=0;i<13;i++) {
        pathTxt+='M'+i*divQuad+',0L'+i*divQuad+','+12*divQuad+'';
    }
    var quadrillage1 = cnt.paper.path(pathTxt).attr({stroke:"#aaa",'stroke-width':'1'});
    quadrillage1.move(5,5);
    pathTxt = '';
    for (i=0;i<3;i++) {
        pathTxt+='M0,'+i*5*divQuad+'L'+12*divQuad+','+i*5*divQuad+'';
    }
    
    for (i=0;i<3;i++) {
        pathTxt+='M'+i*5*divQuad+',0L'+i*5*divQuad+','+12*divQuad+'';
    }
    
    var quadrillage2 = cnt.paper.path(pathTxt).attr({stroke:'#bbb',"stroke-width":'2'});
    quadrillage2.move(5,5);
    //le rectangle
    LeRectangle= cnt.paper.rect(divQuad,divQuad).fill("#33CCFF");
    LeRectangle.attr({'opacity':'0.5','stroke-width':'3','stroke':'#3366BB'});
    LeRectangle.move(5,5);
    rectCorrection = cnt.paper.rect(longueurq*divQuad,largeurq*divQuad).fill("#33FF00");
    rectCorrection.attr({'opacity':'0','stroke-width':'5','stroke':'#3366BB'});
    rectCorrection.move(5,5);
    exo.blocAnimation.append(cnt);
    //Le selecteur drag n drop
    LeCercle = disp.createImageSprite(exo,"LeCercle").css({opacity:0.75});
    LeCercle.css({left:58,top:38});
    exo.blocAnimation.append(LeCercle);
    //
    //exo.btnValider.hide();
    longueure=1;
    largeure=1;
    repEleve=longueure*largeure;
    // Gestion du drag & drop
    LeCercle.draggable({
        containment : '#fond2',
        cursor : 'move',
        drag: function() {
            LeRectangle.attr({'width':LeCercle.position().left-28,'height':LeCercle.position().top-8});
            longueure=Math.round((LeCercle.position().left-28)/divQuad);
            largeure=Math.round((LeCercle.position().top-8)/divQuad);
        },
        //revert:'invalid',
        grid : [30 , 30]
        //start:gestionDrag
    });
    fond2.droppable({
        drop: function( event, ui ) {
            LeRectangle.attr({'width':LeCercle.position().left-28,'height':LeCercle.position().top-8});
            longueure=Math.round((LeCercle.position().left-28)/divQuad);
            largeure=Math.round((LeCercle.position().top-8)/divQuad);
        },
    });
}

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    repEleve = longueure*largeure;
    if( repEleve == 1 ) {
        return "rien"
    } else if( repEleve == soluce ) {
        return "juste"
    } else {
        return "faux"
    }
}

// Correction (peut rester vide)

exo.corriger = function() {
    LeCercle.hide();
    rectCorrection.attr({'opacity':'0.7'});
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    var controle = new disp.createOptControl(exo,{
        type:"radio",
        nom:"totalEssai",
        texte:"Nombre d'essais : ",
        aLabel : ["un","deux"],
        aValeur : [1,2]
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"longueur",
        texte:"Longueur : "
    });
    exo.blocParametre.append(controle);
    var controle = new disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        //taille:50,
        nom:"largeur",
        texte:"Largeur : "
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))