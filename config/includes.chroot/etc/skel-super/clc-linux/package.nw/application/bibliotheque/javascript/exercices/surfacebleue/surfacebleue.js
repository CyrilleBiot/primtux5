var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.surfacebleue = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var cnt, cntsvg, aNombre, divQuad, LeRectangle, etiquetteQuestion1, etiquetteQuestion2, champReponse, nombre1, nombre2, textSolution, repEleve, soluce, sommeTotale;
var xx1,xx2,yy1,yy2,sol1,sol2;
// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    txt : "surfacebleue/textes/surfacebleue_fr.json",
    illustration : "surfacebleue/images/illustration.png"
};
// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
		typeCalcul:0
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    aNombre=[];
    var aN=[];
    var aNombresA=[];
    var aNombresB=[];
    var aNombresC=[];
    var aNombresD=[];
    var i, j, lengthA, lengthB, nA, nB, e;
    if (exo.options.typeCalcul===0||exo.options.typeCalcul==1) {
		if (exo.options.typeCalcul===0) {
		    aNombresA = [4,5,6,7];
		    aNombresB = [5,6,7,8,9,10];
		} 
		else {
		    aNombresA = [6,7,8,9,10];
		    aNombresB = [5,6,7,8,9,10];
		}
		lengthA = aNombresA.length;
		lengthB = aNombresB.length;
		for( i = 0 ; i < lengthA ; i++ ) {
		    nA = aNombresA[i];
		    for ( j = 0 ; j < lengthB ; j++ ) {
				nB = aNombresB[j];
				aN.push([nA,nB]);
		    }
		}
		e=[];
		for (i=0;i<exo.options.totalQuestion;i++) {
			e[i]=Math.floor(Math.random()*aN.length);
		    for (j=0;j<i;j++) {
			    if (e[i]==e[j]) {
			    	i--;
				}
		    }
		}
		//e.sort(function(x, y) {return x - y;});
		util.shuffleArray(e);
		console.log(e);
		for (i=0;i<exo.options.totalQuestion;i++) {
		    aNombre[i]=aN[e[i]];
		}
	}
	else if (exo.options.typeCalcul==3) {
		aNombresA = [7,8,9,10];
		aNombresB = [7,8,9,10];
		aNombresC = [3,3,4,5];
		aNombresD = [3,4,4,5];
		lengthA = aNombresA.length;
		lengthB = aNombresB.length;
		for( i = 0 ; i < lengthA ; i++ ) {
		    nA = aNombresA[i];
		    for ( j = 0; j < lengthB ; j++ ) {
				nB = aNombresB[j];
				var nC = aNombresC[j];
				var nD = aNombresD[j];
				aN.push([nA,nB,nC,nD]);
		    }
		}
		console.log(aN);
		e=[];
		for (i=0;i<exo.options.totalQuestion;i++) {
			e[i]=Math.floor(Math.random()*aN.length);
			console.log("i=",i,"e[i]",e[i]);
		    for (j=0; j<i; j++) {
		    	console.log("j=",j,"e[j]",e[j]);
			    if (e[i]==e[j]) {
			    	i--;
				}
		    }
		}
		//e.sort(function(x, y) {return x - y;});
		util.shuffleArray(e);
		for (i=0; i<exo.options.totalQuestion; i++) {
		    aNombre[i]=aN[e[i]];
		}
    }
};
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    exo.keyboard.config({
		numeric : "enabled ",
		arrow : "disabled ",
		large : "disabled ",
    });
    //consigne
    etiquetteQuestion1 = disp.createTextLabel(exo.txt.consigne1);
    exo.blocAnimation.append(etiquetteQuestion1);
    etiquetteQuestion1.css({
        left:20,
        top:360,
        fontWeight:200
    });
    champReponse = disp.createTextField(exo,3);
    exo.blocAnimation.append(champReponse);
    champReponse.position({
        my:'left center',
        at:'right+6 center',
        of:etiquetteQuestion1
    });
    champReponse.focus();
  

    //le quadrillage
    cnt = disp.createSvgJsContainer(430,325).css({left:100,top:20});
    //
    
    divQuad=21;
    var lim1=0, lim2=0, lim3=0, lim4=0;
    var couleurFond = "#fefefe", couleurForme = "#039be5";
    sommeTotale=0;
    var carre;
    if (exo.options.typeCalcul === 0||exo.options.typeCalcul==1) {
		if (q === 0 || q==1||q==2||q==3||q==4) { //un simple rectangle
			nombre1=15;
			nombre2=15;
			lim1=aNombre[q][0];
			lim2=aNombre[q][1];
			for (i=0; i<nombre1; i++) {
				for (j=0; j<nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>2&&i<2+lim1&&j>3&&j<3+lim2) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			xx1 = 5+3*divQuad;
			xx2 = 5+(2+lim1)*divQuad;
			yy1 = 5+4*divQuad;
			yy2 = 5+(3+lim2)*divQuad;
			sommeTotale = (lim1-1)*(lim2-1);
			lim1 = lim1-1;
			lim2 = lim2-1;
			sol1 = lim1;
			sol2 = lim2;
			textSolution = ""+lim1+" x "+lim2+" = "+sommeTotale;
		}
    }
    else if (exo.options.typeCalcul == 2) {
		if (q===0) {//un T couché
			nombre1=Math.floor(3*Math.random()+11);
			nombre2=Math.floor(2*Math.random()+8);
			for (i=0; i<=nombre1+1; i++) {
				for (j=0; j<=nombre2+1; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>0 && i<nombre1+1 && j>3 && j<7) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (j>0 && j<nombre2+1 && i>0 && i<4) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			sommeTotale = (nombre1-3)*3+nombre2*3;
			textSolution = "("+(nombre1-3)+" x 3) + ("+(nombre2)+" x 3)";
		}
		else if (q==1) {//un H
			nombre1=14;
			nombre2=14;
			lim1=Math.floor(2*Math.random()+8);
			lim2=Math.floor(2*Math.random()+4);
			for (i=0; i<=nombre1+1; i++) {
				for (j=0; j<nombre2+1; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (j>0 && j<lim1+1 && i>0 && i<4) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (j>3 && j<7 && i>3 && i<4+lim2) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (j>0 && j<lim1+1 && i>3+lim2 && i<7+lim2) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			sommeTotale = 2*lim1*3+lim2*3;
			textSolution = "(2 x "+lim1+" x 3) + ("+lim2+" x 3)";
		}
		else if (q==2) {// deux rectangles 
			nombre1=Math.floor(0*Math.random()+15);
			nombre2=Math.floor(0*Math.random()+13);
			lim1=Math.floor(2*Math.random()+3);
			lim2=Math.floor(2*Math.random()+3);
			lim3=Math.floor(2*Math.random()+4);
			lim4=Math.floor(2*Math.random()+7);
			for (i=0; i<nombre1; i++) {
				for (j=0; j<nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>1&&i<1+lim1&&j>2&&j<2+lim2) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>7&&i<7+lim3&&j>4&&j<4+lim4) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			sommeTotale = (lim1-1)*(lim2-1)+(lim3-1)*(lim4-1);
			lim1 = lim1-1;
			lim2 = lim2-1;
			lim3 = lim3-1;
			lim4 = lim4-1;
			textSolution = "("+lim1+" x "+lim2+") + ("+lim3+" x "+lim4+")";
		}
		else if (q==3) {//4 carrés identiques
			nombre1=Math.floor(0*Math.random()+16);
			nombre2=Math.floor(0*Math.random()+14);
			lim1=Math.floor(2*Math.random()+5);
			for (i=0; i<nombre1; i++) {
				for (j=0; j<nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>1&&i<lim1+1&&j>1&&j<lim1+1) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>15-lim1&&i<15&&j>1&&j<lim1+1) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>15-lim1&&i<15&&j>13-lim1&&j<13) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>1&&i<lim1+1&&j>13-lim1&&j<13) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			sommeTotale = 4*(lim1-1)*(lim1-1);
			lim1 = lim1-1;
			textSolution = "4 x "+lim1+" x "+lim1+"";
		}
		else if (q==4) {//deux yeux une bouche
			nombre1=Math.floor(0*Math.random()+17);
			nombre2=Math.floor(0*Math.random()+14);
			lim1=Math.floor(2*Math.random()+4);
			for (i=0; i<nombre1; i++) {
				for (j=0; j<nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>2&&i<2+lim1&&j>1&&j<1+lim1) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>16-lim1&&i<16&&j>1&&j<1+lim1) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>5&&i<5+8&&j>13-lim1&&j<13) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			sommeTotale = 2*(lim1-1)*(lim1-1)+7*(lim1-1);
			lim1=lim1-1;
			textSolution = "(2 x "+lim1+" x "+lim1+") + (7 x "+lim1+")";
		}
    }
    else if (exo.options.typeCalcul == 3) {
		if (q===0 || q==1 || q==2) {//un rectangle évidé (soustraction)
			nombre1=Math.floor(0*Math.random()+17);
			nombre2=Math.floor(0*Math.random()+15);
			lim1=aNombre[q][0];
			lim2=aNombre[q][1];
			lim3=aNombre[q][2];
			lim4=aNombre[q][3];
			for (i=1; i<=nombre1; i++) {
				for (j=1; j<=nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>3&&i<3+lim1&&j>3&&j<3+lim2) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>5&&i<5+lim3&&j>6&&j<6+lim4) {
						carre.attr({fill:'#fefefe',opacity:0.25});
					}
				}
			}
			sommeTotale = (lim1-1)*(lim2-1)-(lim3-1)*(lim4-1);
			lim1 = lim1-1;
			lim2 = lim2-1;
			lim3 = lim3-1;
			lim4 = lim4-1;
			textSolution = "("+lim1+" x "+lim2+") - ("+lim3+" x "+lim4+")";
		}
		if (q==3) { //rectangle avec des bandes à l'intérieur
			nombre1=Math.floor(0*Math.random()+17);
			nombre2=Math.floor(0*Math.random()+15);
			lim1=aNombre[q][0];
			lim2=aNombre[q][1];
			for (i=1; i<=nombre1; i++) {
				for (j=1; j<=nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (i>3&&i<3+lim1&&j>3&&j<3+lim2) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>4&&i<6&&j>4&&j<2+lim2) {
						carre.attr({fill:'#fefefe',opacity:0.25});
					}
					if (i>6&&i<8&&j>4&&j<2+lim2) {
						carre.attr({fill:'#fefefe',opacity:0.25});
					}
					if (i>8&&i<10&&j>4&&j<2+lim2) {
						carre.attr({fill:'#fefefe',opacity:0.25});
					}
				}
			}
			sommeTotale = (lim1-1)*(lim2-1)-3*(lim2-3);
			lim1 = lim1-1;
			lim3 = lim2-3;
			lim2 = lim2-1;
			textSolution = "("+lim1+" x "+lim2+") - (3 x "+lim3+")";
		}
		if (q==4) { //rectangle avec des bandes à l'intérieur
			nombre1=Math.floor(3*Math.random()+9);
			nombre2=Math.floor(3*Math.random()+9);
			for (i=1; i<=nombre1; i++) {
				for (j=1; j<=nombre2; j++) {
					carre = cnt.paper.rect(divQuad,divQuad).attr({fill:couleurFond,opacity:0.25,strokeWidth:1,stroke:couleurForme});
					carre.x(5+divQuad*i);
					carre.y(5+divQuad*j);
					if (j>3&&j<6) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
					if (i>3&&i<7) {
						carre.attr({fill:couleurForme,strokeWidth:0,opacity:1});
					}
				}
			}
			sommeTotale = nombre1*2 + nombre2*3 - 2*3;
			console.log("Somme Totale:",sommeTotale,"nombre1",nombre1,"nombre2",nombre2);
			textSolution = "("+nombre1+" x "+nombre2+") - ("+nombre1+" - "+3+") x ("+nombre2+" - "+2+")";
			console.log(textSolution);
		}
    }
    exo.blocAnimation.append(cnt);
    //exo.btnValider.hide();
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    repEleve = Number(champReponse.val());
    console.log(repEleve);
    console.log(sommeTotale);
    if( repEleve === "") {
        return "rien";
    } 
    else if( repEleve == sommeTotale ) {
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    var correction = disp.createCorrectionLabel(sommeTotale);
    exo.blocAnimation.append(correction);
    correction.position({
        my:"center center",
        at:"center center+40",
        of:champReponse,
    });
    var barre1 = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre1);
    var etiquetteSolution = disp.createTextLabel(textSolution);
    exo.blocAnimation.append(etiquetteSolution);
    etiquetteSolution.css({
        left:400,
        top:160
    });
    if (exo.options.typeCalcul === 0||exo.options.typeCalcul==1) {
		var fleche1 = cnt.paper.polyline([[xx1+4,yy1-14],[xx1,yy1-10],[xx1+4,yy1-6]]);
		var fleche11 = cnt.paper.line(xx1,yy1-10,xx2,yy1-10).stroke({ width: 1 });
		var fleche12 = cnt.paper.polyline([[xx2-4,yy1-14],[xx2,yy1-10],[xx2-4,yy1-6]]);
		var fleche2 = cnt.paper.polyline([[xx1-14,yy1+4],[xx1-10,yy1],[xx1-6,yy1+4]]);
		var fleche21 = cnt.paper.line(xx1-10,yy1,xx1-10,yy2).stroke({ width: 1 });
		var fleche22 = cnt.paper.polyline([[xx1-14,yy2-4],[xx1-10,yy2],[xx1-6,yy2-4]]);
		var etiquetteSolution1 = disp.createTextLabel(sol1);
		exo.blocAnimation.append(etiquetteSolution1);
		var left1 =(xx2+xx1)/2+92;
		var top1 = yy1-20;
		etiquetteSolution1.css({
		    left:left1,
		    top:top1
		});
		var etiquetteSolution2 = disp.createTextLabel(sol2);
		exo.blocAnimation.append(etiquetteSolution2);
		var left2 = xx1+70;
		var top2 = (yy2+yy1)/2+5;
		etiquetteSolution2.css({
		    left:left2,
		    top:top2
		});
    }
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle;
    //
    controle = new disp.createOptControl(exo,{
        type:"radio",
        nom:"totalEssai",
        texte:exo.txt.option2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:36,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.option3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"typeCalcul",
        texte:exo.txt.option4,
        aLabel:exo.txt.label4,
        aValeur:[0,1,2,3]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));