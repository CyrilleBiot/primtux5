var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.approximationsomme = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aNombre,aCarte,aValeurCarte,gererClavier,pas,soluce,curseur,index;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:10,
        totalEssai:1,
        tempsExo:0,
        niveau:2
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "approximationsomme/textes/approximationsomme_fr.json",
    curseur : "approximationsomme/images/curseur.png",
    carte : "approximationsomme/images/carte.png",
    illustration:"approximationsomme/images/illustration.jpg",
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    var n = exo.options.niveau;
    var q = exo.options.totalQuestion;
    aNombre=[];
    if(n<6){
        aNombre = creerNombre(n,q);
    }
    else if(n == 6){
        for ( var j = 0 ; j < q ; j++ ) {
            aNombre.push(creerNombre(j % 5 + 1,1)[0]);
        }
        console.log(aNombre)
    }
    

    //
    function creerNombre(niveau,nbreQuestion)
    {
        var nCentaineInf = 0;
        var soluceTemp;
        var nA,nB,i,arrondiA,arrondiB;
        var aCouple = [];
        if ( niveau==1 )
        {
            // addition 3 chiffres + 2 chiffres
            // arrondir à la dizaine superieure
            // pas de retenue dans l'ajout des centaines et des dizaines
            // planetes 100, 200, ..., 1 000
            aValeurCarte = [1000,900,800,700,600,500,400,300,200,100];
            
            for (i=0;i<nbreQuestion;i++)
            {
                cA = 0 ;
                cB = 1 + Math.floor(Math.random()*(9-1+1));
                dA = 1 + Math.floor(Math.random()*(9-1+1));
                dB = 1 + Math.floor(Math.random()*(9-1+1));
                uA = 1 + Math.floor(Math.random()*(9-1+1));
                uB = 1 + Math.floor(Math.random()*(9-1+1));
                nA = 100*cA+10*dA+uA;
                arrondiA = Math.round((10*dA+uA)/10)*10;
                nB = 100*cB+10*dB+uB;
                arrondiB = Math.round((10*dB+uB)/10)*10;
                //soluceTemp = Math.round((nA+nB)/100)*100;
                // centaine inférieure
                if(i<Math.floor(nbreQuestion/2)){
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB<50 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
                // centaine supérieure
                else {
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB> 50 && arrondiA+arrondiB<100 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
            }
            
        }
        else if ( niveau==2 )
        {
            // addition 3 chiffres + 2 chiffres
            // arrondir à la dizaine superieure
            // avec retenue dans l'ajout des dizaines
            // planetes 100, 200, ..., 1 000
            aValeurCarte = [1000,900,800,700,600,500,400,300,200,100];
            
            for (i=0;i<nbreQuestion;i++)
            {
                cA = 0 ;
                cB = 1 + Math.floor(Math.random()*(9-1+1));
                dA = 1 + Math.floor(Math.random()*(9-1+1));
                dB = 1 + Math.floor(Math.random()*(9-1+1));
                uA = 1 + Math.floor(Math.random()*(9-1+1));
                uB = 1 + Math.floor(Math.random()*(9-1+1));
                nA = 100*cA+10*dA+uA;
                arrondiA = Math.round((10*dA+uA)/10)*10;
                nB = 100*cB+10*dB+uB;
                arrondiB = Math.round((10*dB+uB)/10)*10;
                //soluceTemp = Math.round((nA+nB)/100)*100;
                // centaine inférieure
                if(i<Math.floor(nbreQuestion/2)){
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB<50 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
                // centaine supérieure avec retenue éventuelle
                else {
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB>50 && arrondiA+arrondiB < 150 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
            }
            
        }
        else if ( niveau==3 )
        {
            // addition 3 chiffres + 3 chiffres
            // arrondir à la dizaine superieure
            // planetes 100, 200, ..., 1 000
            aValeurCarte = [1000,900,800,700,600,500,400,300,200,100];
            for (i=0;i<nbreQuestion;i++)
            {
                cA = 1 + Math.floor(Math.random()*(9-1+1));
                cB = 1 + Math.floor(Math.random()*(9-1+1));
                dA = 1 + Math.floor(Math.random()*(9-1+1));
                dB = 1 + Math.floor(Math.random()*(9-1+1));
                uA = 1 + Math.floor(Math.random()*(9-1+1));
                uB = 1 + Math.floor(Math.random()*(9-1+1));
                nA = 100*cA+10*dA+uA;
                arrondiA = Math.round((10*dA+uA)/10)*10;
                nB = 100*cB+10*dB+uB;
                arrondiB = Math.round((10*dB+uB)/10)*10;
                soluceTemp = Math.round((nA+nB)/100)*100;
                if(i<Math.floor(nbreQuestion/2)){
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB<50 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
                else {
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && (arrondiA+arrondiB)%50 != 0 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
            }
            
        }
        else if ( niveau==4 )
        {
            // addition 4 chiffres + 3 chiffres
            // arrondir à la centaine superieure
            // planetes 1000, 2000, ..., 10 000
            aValeurCarte = [10000,9000,8000,7000,6000,5000,4000,3000,2000,1000];
            var count=0;
            for (i=0;i<nbreQuestion;i++)
            {
                mA = 0 ;
                mB = 1 + Math.floor(Math.random()*(9-1+1));
                cA = 1 + Math.floor(Math.random()*(9-1+1));
                cB = 1 + Math.floor(Math.random()*(9-1+1));
                dA = 1 + Math.floor(Math.random()*(9-1+1));
                dB = 1 + Math.floor(Math.random()*(9-1+1));
                uA = 1 + Math.floor(Math.random()*(9-1+1));
                uB = 1 + Math.floor(Math.random()*(9-1+1));
                nA = 1000*mA+100*cA+10*dA+uA;
                arrondiA = Math.round((100*cA+10*dA+uA)/100)*100;
                nB = 1000*mB+100*cB+10*dB+uB;
                arrondiB = Math.round((100*cB+10*dB+uB)/100)*100;
                
                // millier inférieur
                if(i<Math.floor(nbreQuestion/2)){

                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && mA+mB < 10 && arrondiA+arrondiB<500 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
                // millier supérieur avec retenue éventuelle
                else {
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && mA+mB<10 && arrondiA+arrondiB>500 && (arrondiA+arrondiB)%500 != 0 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
            }
            
        }
        else if ( niveau==5 )
        {
            // addition 3 chiffres + 3 chiffres
            // arrondir à la dizaine superieure
            // planetes 100, 200, ..., 1 000
            aValeurCarte = [10000,9000,8000,7000,6000,5000,4000,3000,2000,1000];
            for (i=0;i<nbreQuestion;i++)
            {
                mA = 1 + Math.floor(Math.random()*(9-1+1));
                mB = 1 + Math.floor(Math.random()*(9-1+1));
                cA = 1 + Math.floor(Math.random()*(9-1+1));
                cB = 1 + Math.floor(Math.random()*(9-1+1));
                dA = 1 + Math.floor(Math.random()*(9-1+1));
                dB = 1 + Math.floor(Math.random()*(9-1+1));
                uA = 1 + Math.floor(Math.random()*(9-1+1));
                uB = 1 + Math.floor(Math.random()*(9-1+1));
                nA = 1000*mA+100*cA+10*dA+uA;
                arrondiA = Math.round((100*cA+10*dA+uA)/100)*100;
                nB = 1000*mB+100*cB+10*dB+uB;
                arrondiB = Math.round((100*cB+10*dB+uB)/100)*100;
                if(i<Math.floor(nbreQuestion/2)){
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && mA+mB<10 && arrondiA+arrondiB<500 && util.find([nA,nB,soluceTemp],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
                else {
                    if (
                        nA%5 !== 0 && nB % 5 !== 0 && mA+mB<10 && (arrondiA+arrondiB)%500 != 0 && util.find([nA,nB],aCouple) < 0
                    ) 
                    {
                        aCouple.push([nA,nB]);
                    } 
                    else {
                        i--;
                    }
                }
            }
            
        }
        util.shuffleArray(aCouple);
        return aCouple;
    }
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    console.log(aNombre);
    exo.keyboard.config({
        numeric:"disabled"
    });
    //
    var q = exo.indiceQuestion;
    pas = aNombre[q][1] < 1000 ? 100 :1000;
    aValeurCarte = [10*pas,9*pas,8*pas,7*pas,6*pas,5*pas,4*pas,3*pas,2*pas,pas];
    // Les cartes
    var carte;
    aCarte = [];
    for(var i=0;i<10;i++){
        carte = disp.createImageSprite(exo,"carte");
        carte.css({left:250,top:30});
        carte.data({valeur:aValeurCarte[i],selected:false});
        var label = disp.createTextLabel(util.numToStr(aValeurCarte[i]));
        label.css({width:"100%",height:"100%",textAlign:"center",color:"#fff",fontSize:18,lineHeight:"30px"});
        carte.append(label);
        exo.blocAnimation.append(carte);
        // l'animation qui fait tomber les cartes
        carte.transition({y:i*(carte.height()+5)});
        aCarte[i]=carte;
    }
    // le curseur
    curseur = disp.createImageSprite(exo,"curseur");
    curseur.css({left:50,top:30});
    var sA = util.numToStr(aNombre[q][0]);
    var sB = util.numToStr(aNombre[q][1]);
    var sOperation = q%2 === 0 ? sB + " + " + sA : sA + " + " + sB;
    var lblOperation = disp.createTextLabel(sOperation);
    lblOperation.css({height:"100%",width:curseur.width()-10,fontSize:20,textAlign:"center",lineHeight:"66px"});
    curseur.append(lblOperation);
    exo.blocAnimation.append(curseur);
    
    repEleve = 1*pas;
    index = 9;
    
    
    // on ajoute la gestion du clavier
    gererClavier = function (e){
        if ( e.which == 40 && curseur.position().top < 310 ) {
            //console.log("bas");
            curseur.css({top:"+=36px"});
            repEleve-=pas;
            index++;
        }
        else if ( e.which == 38 && curseur.position().top > 30 ){
            //console.log("haut");
            curseur.css({top:"-=36px"});
            repEleve+=pas;
            index--;
        }
        for(var i=0;i<10;i++){
            aCarte[i].css({boxShadow:'none'});
        }
        aCarte[index].css({boxShadow:'0px 0px 3px 5px #FF9933'});
        console.log(repEleve);
        return false;
    };
    $(document).on("keydown.clc",gererClavier);
    // la consigne
    var lblConsigne = disp.createTextLabel(exo.txt.consigne2);
    lblConsigne.css({
        width:200,left:460,top:80,fontSize:16,lineHeight:1.5, 
        fontWeight:200, padding:10,border:"3px solid #3399FF",
        backgroundColor:"#fff"
    });
    
    // l'animation qui fait tomber l curseur
    curseur.transition({y:310},800,"easeOutBack",function(){
        aCarte[index].css({boxShadow:'0px 0px 3px 5px #FF9933'});
        exo.blocAnimation.append(lblConsigne);
    });

};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    $(document).off("keydown.clc",gererClavier);
    var q = exo.indiceQuestion;
    soluce = Math.round((aNombre[q][0]+aNombre[q][1])/pas)*pas;
    console.log(repEleve,soluce);
    if(repEleve == soluce){
        return "juste";
    }
    else {
        return "faux";
    }
    
};

// Correction (peut rester vide)
exo.corriger = function() {
    for(var i=0;i<10;i++){
        if(aCarte[i].data().valeur != soluce){
            aCarte[i].css({opacity:0.5});
        }
        else{
            aCarte[i].css({boxShadow:'0px 0px 3px 5px #FF0000'});
            var dif  = i-index >= 0 ? "+="+Math.abs((i-index)*36)+"px" : "-="+Math.abs((i-index)*36)+"px";
            console.log(dif);
            curseur.transition({y:dif});
        }
        
    }
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:36,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"niveau",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[1,2,3,4,5,6]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));