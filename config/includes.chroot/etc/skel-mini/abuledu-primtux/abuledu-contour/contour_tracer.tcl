#!/bin/sh
#contour.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $Id: play_contour.tcl,v 1.9 2006/03/25 00:55:15 abuledu_francois Exp $
#  Author  : andre.connes@wanadoo.fr
#  Date    : 05/01/2003 Modification : 16/02/2004
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     Andre Connes
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
# 
#  *************************************************************************

global fichier_image
source contour.conf
source msg.tcl
source fin_sequence.tcl
source lanceapplication.tcl

set dossier_images [lindex $argv 0]
set fichier_image [lindex $argv 1]

  #
  # langue par defaut
  #
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

  #
  # taille des numeros par defaut
  #
  if {![file exists [file join  $glob(home_reglages) charsize.conf]]} {
    set f [open [file join $glob(home_reglages) charsize.conf] "w"]
    puts $f 9
    close $f
  }
  set f [open [file join $glob(home_reglages) charsize.conf] "r"]
  gets $f glob(charSize)
  close $f

  set sysFont "Courier $glob(charSize) normal"

  #
  # epaisseur des traits par defaut
  #
  if {![file exists [file join  $glob(home_reglages) epaisseur.conf]]} {
    set f [open [file join $glob(home_reglages) epaisseur.conf] "w"]
    puts $f 2
    close $f
  }
  set f [open [file join $glob(home_reglages) epaisseur.conf] "r"]
  gets $f glob(epaisseur)
  close $f

  #
  # couleur des numeros et des traits
  #
  set f [open [file join $glob(home_reglages) couleur.conf] "r"]
  gets $f glob(couleur)
  close $f

  #
  # numero de depart
  #
  set f [open [file join $glob(home_reglages) depart.conf] "r"]
  gets $f d
  set glob(depart) [expr $d-0]
  close $f

  #
  # pas de progression
  #
  set f [open [file join $glob(home_reglages) progression.conf] "r"]
  gets $f glob(progression)
  close $f

  #
  #mode de selection des points a la souris : survol ou clic ou double-clic ?
  #
  set f [open [file join $glob(home_reglages) mode_souris.conf] "r"]
  gets $f glob(modeSouris) ;# Survoler, Cliquer, Double-cliquer
  close $f
  if { $glob(modeSouris) == "Cliquer" } {
    set modeSouris Button-1
  } elseif { $glob(modeSouris) == "Double-cliquer" } {
    set modeSouris Double-ButtonPress-1
  } else {
    set modeSouris Enter
  }

  package require Img

##########################################################

proc color { c } {
  # traduction fr <-> en
  global glob
  set i [lsearch $glob(couleurs) $c]
  return [lindex $glob(colors) $i]
}

####################################################
#
# affiche_image
#
####################################################

proc affiche_image { } {
  global glob wbgimage hbgimage bgimage 
  .frame.can_image delete all
  .frame.can_image create image \
	[expr $glob(org) + int($wbgimage/2)+1] [expr $glob(org) + int($hbgimage/2)+1] -image $bgimage
  .bframe.but_ok configure -state disabled
}

####################################################
#
# point_suivant
#
####################################################

proc point_suivant_entry { } {
  global glob
  set n [.bframe.ent_numero get]
  set numero [expr ($n - $glob(depart)) / $glob(progression) + 1]
  if { $numero == $glob(npoint) } {
    tracer_segment
  } else {
    .bframe.ent_numero configure -bg red
    after 200 { set attente_erreur non }
    vwait attente_erreur
    .bframe.ent_numero configure -bg white
  }
  .bframe.ent_numero delete 0 end
} ;# point_suivant_entry

proc point_suivant_souris { x y } {
  global glob
  if { $glob(modeSouris) == "Pas-de-souris" } return
  set p3 [expr 3*$glob(npoint)]
  set list_points [.frame.can_image find overlapping [expr $x -7] [expr $y -7] [expr $x +7] [expr $y +7]]
  if { [lsearch $list_points $p3] >= 0 } {
    tracer_segment
  } else {
    .frame.can_image create oval [expr $x -10]  [expr $y-10] [expr $x +10] [expr $y +10] -width 3 -fill red \
    -tag erreur
    after 200 { set attente_erreur non }
    vwait attente_erreur
    .frame.can_image delete erreur
  }
} ;# point_suivant_souris
  
    
proc tracer_segment { } {
  global glob
    .frame.can_image delete oval($glob(npoint))
    if { ! $glob(bool_premier) } {
      set i [expr $glob(vpoint) -1]
      set x0 [lrange $glob(llist_points) [expr 2*$i-1] [expr 2*$i-1]]
      set y0 [lrange $glob(llist_points) [expr 2*$i-0] [expr 2*$i-0]]
      set x1 [lrange $glob(llist_points) [expr 2*$i+1] [expr 2*$i+1]]
      set y1 [lrange $glob(llist_points) [expr 2*$i+2] [expr 2*$i+2]]
      .frame.can_image create line $x0 $y0 $x1 $y1 -width $glob(epaisseur) \
        -fill [color $glob(couleur)]
      set x2 [lrange $glob(llist_points) [expr 2*$i+3] [expr 2*$i+3]]
      set y2 [lrange $glob(llist_points) [expr 2*$i+4] [expr 2*$i+4]]
      catch { .frame.can_image delete numero([expr $glob(npoint) -1]) }
      incr glob(vpoint) 1
      incr glob(npoint) 1
      if { [expr $x2 + $y2] == 0 } {
        # sentinelle
        catch { .frame.can_image delete numero([expr $glob(npoint) -1]) }
        set glob(bool_premier) 1
        incr glob(vpoint) 1
      }
    } else {
      # premier point d'un contour
      set glob(bool_premier) 0
      set i [expr $glob(vpoint) -0]
      set x [lrange $glob(llist_points) [expr 2*$i-1] [expr 2*$i-1]]
      set y [lrange $glob(llist_points) [expr 2*$i-0] [expr 2*$i-0]]
      .frame.can_image create oval [expr $x -10]  [expr $y -10] [expr $x +10] [expr $y +10] -width 3 -fill green \
      -tag depart
      after 200 { set attente_depart non }
      vwait attente_depart
      .frame.can_image delete depart
      .frame.can_image delete numero($glob(npoint))
      incr glob(vpoint) 1
      incr glob(npoint) 1
    }
  if { $glob(vpoint) > $glob(npoints_total) } {
    catch { .frame.can_image delete numero([expr $glob(npoint) -1]) }
    .bframe.but_ok configure -state normal
    .bframe.ent_numero configure -state disabled
  }
} ;# tracer_segment

# #################################################################################
#
#                                   affiche_points
#
# #################################################################################

proc affiche_points { } {
  global glob dossier_images fichier_image bgimg wbgimage hbgimage bgimage fichier_image sysFont modeSouris
  .frame.can_image delete all
  
  # afficher le fond, fonction de l'image de la liste des images sélectionnées

  set bgimg [file join [pwd] images $dossier_images $fichier_image]
  
  set bgimage [image create photo -file [file join images $bgimg]]

  set hbgimage [image height $bgimage]
  set wbgimage [image width $bgimage]

  # lire les caracteristiques de cette image : nb.lignes/colonnes + zones
  set nom_image [file rootname [file tail $bgimg]]
  catch {  
    set f [open [file join [pwd] points $dossier_images $nom_image] "r"] 
    set glob(list_points) [gets $f]
    close $f
  }
  if { $glob(list_points) ==""} {
    tk_messageBox -message [mc "Pas de points dans l'image"]
    return
  }

  # verifier si ancienne version
  if { [string first " 0 0" $glob(list_points)] <= 0 } {
    append glob(list_points) " 0 0"
  }
  # placer les points sauf le dernier
  set glob(llist_points) [split $glob(list_points)]
  set glob(npoints_total) [expr [llength $glob(llist_points)] / 2]
  set vpoint 1 ;# numero de point virtuel (inclus les sentinelles 0 0)
  set npoint 1 ;# numero de point reel (sans les sentinelles)
  while {$vpoint < [expr $glob(npoints_total)-1] } {
    set x [lrange $glob(llist_points) [expr 2*$vpoint-1] [expr 2*$vpoint-1]]
    set y [lrange $glob(llist_points) [expr 2*$vpoint-0] [expr 2*$vpoint-0]]
    if {[expr $x + $y] != 0 } {
      # pas une sentinelle
      .frame.can_image create oval [expr $x -0]  [expr $y-0] [expr $x +1] [expr $y +1] -fill blue
      if { $glob(modeSouris) != "Pas-de-souris" } {
        .frame.can_image create text [expr $x -5] [expr $y -2] \
          -text [expr $glob(depart)+$glob(progression)*($npoint-1)] \
          -fill [color $glob(couleur)] \
          -tag numero($npoint) \
          -font $sysFont
        .frame.can_image create oval [expr $x -7]  [expr $y-7] [expr $x +7] [expr $y +7] -width 0 \
          -tag oval($npoint)
        .frame.can_image bind oval($npoint) <$modeSouris> "point_suivant_souris $x $y"
      }
      incr npoint 1
    }
    incr vpoint 1
  }
  #dernier point de l'autre cote au cas ou boucle fermee
  set x [lrange $glob(llist_points) [expr 2*$vpoint-1] [expr 2*$vpoint-1]]
  set y [lrange $glob(llist_points) [expr 2*$vpoint-0] [expr 2*$vpoint-0]]
  .frame.can_image create oval [expr $x -0]  [expr $y-0] [expr $x +1] [expr $y +1] -fill blue
  if { $glob(modeSouris) != "Pas-de-souris" } {
    .frame.can_image create text [expr $x +9] [expr $y -2] \
          -text [expr $glob(depart)+$glob(progression)*($npoint-1)] \
          -fill [color $glob(couleur)] \
          -tag numero($npoint) \
          -font $sysFont
    .frame.can_image create oval [expr $x -7]  [expr $y-7] [expr $x +7] [expr $y +7] -width 0 \
      -tag oval($npoint)
    .frame.can_image bind oval($npoint) <$modeSouris> "point_suivant_souris $x $y"
  }
  # initialiser le numero du premier point a devoiler (Le premier sera 1)
  # increntatation de 2 et 1 respectivement pour le premier point
  set glob(vpoint) 1
  set glob(npoint) 1
  set glob(bool_premier) 1
  # c'est parti !

} ;# affiche_points

#####################################################################################
#
# fenetre principale
#
#      placement de la fenetre en haut et a gauche de l'ecran
#
#####################################################################################

#wm geometry . [expr [winfo screenwidth .]-10]x[expr [winfo screenheight .]-80]+0+0
wm geometry . 795x580+0+0
. configure -bg $glob(bgcolor)

frame .frame -bg $glob(bgcolor)
pack .frame -side top -fill both -expand yes

  # afficher le nom du theme
  wm title . "Contour [file rootname $fichier_image] : [mc $glob(modeSouris)] -> $glob(depart) + $glob(progression)"

# ################ On cree un texte de présentation ##################################
# a gauche

label .frame.lab_presente -text "Joins les points\ntu verras,\n\nUne image\npour toi et moi." \
  -width 20 -bg $glob(bgcolor) -foreground #ffff00 -font { Courier 15 italic bold }
pack .frame.lab_presente -side left ;#-expand true

# ################ On cree un canvas image ###########################################
# a droite

canvas .frame.can_image -width 606 -height 496 -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
#canvas .frame.can_image -width 794 -height 496 -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
pack .frame.can_image -side left -expand true 

# ############# on cree une frame ok, pouce, gagne, porte ##########################
# en bas

frame .bframe -bg $glob(bgcolor)
pack .bframe -side bottom -expand true

  entry .bframe.ent_numero -width 5 -relief sunken
  grid .bframe.ent_numero -padx 10 -column 1 -row 1
  bind .bframe.ent_numero <Return> "point_suivant_entry"
  bind .bframe.ent_numero <KP_Enter> "point_suivant_entry"
  focus .bframe.ent_numero
  
  button .bframe.but_ok -image \
    [image create photo fok \
    -file [file join sysdata ok.png]] \
    -state disabled \
    -command "affiche_image"
  grid .bframe.but_ok -padx 10 -column 2 -row 1

  button .bframe.but_quitter -image \
    [image create photo fquitter \
    -file [file join sysdata quitter_minus.png]] \
    -command "lanceappli contour.tcl"
  grid .bframe.but_quitter -padx 10 -column 3 -row 1

affiche_points
