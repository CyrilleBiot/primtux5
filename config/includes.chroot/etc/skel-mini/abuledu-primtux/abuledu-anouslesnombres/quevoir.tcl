############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : quevoir.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: quevoir.tcl,v 1.2 2005/12/27 14:28:43 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#calapa.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source polygon.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome


global listdata arg1 arg2 arg3 arg4 gettext cat nbcat listgen

#listgen : liste cat�orie (barque, calapa, etc ...)
#listdata : liste pour le sc�ario en cours
# activit�index_dans_le_sc�ario flag_parcours index_dans_le_parcours
#arg1 : type de activit�(calapa, barque, etc)
#arg2 : num�o de l'activit�dans le sc�ario 
set c .frame.c
set arg1 [lindex $argv 0]
set arg2 [lindex $argv 1]
set arg3 [lindex $argv 2]
set arg4 [lindex $argv 3]


#interface
. configure -background #ffff80
frame .frame -width 640 -height 390 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 390 -background #ffff80 -highlightbackground #ffff80
pack $c

#ouverture du fichier calapa.conf ou barque.conf ...
set ext .conf
set f [open [file join $Home reglages $arg1$ext] "r"]
set listgen [gets $f]
close $f

image create photo bien -file [file join sysdata pbien.gif] 
image create photo pass -file [file join sysdata ppass.gif]
image create photo mal -file [file join sysdata pmal.gif]
image create photo neutre -file [file join sysdata pneutre.gif]

#procedures
####################################################""
proc recommence {c} {
#after cancel animation $c
place $c
}

proc afficheitem {c} {
global tid visible nbvoir
variable tforme
variable tcouleur
variable ttaille
incr nbvoir
.mframe.consigne configure -text " "
foreach id [array names tid] {
$c itemconfigure $id -state hidden
}
set listvar ""
if {$tforme !="null" && $tforme !=""} {
lappend listvar $tforme
}
if {$tcouleur !="null" && $tcouleur !=""} {
lappend listvar $tcouleur
}
if {$ttaille !="null" && $ttaille !=""} {
lappend listvar $ttaille
}
if {[llength $listvar] == 0} {
bell
return
}
set listtmp ""
set str ""
	switch [llength $listvar] {
		1 {
			foreach id [array names tid] {
				if {[lsearch $tid($id) [lindex $listvar 0]] != -1} {
				lappend listtmp $id
				}
			}
			append str [mc [lindex $listvar 0]]
		}

		2 {
			foreach id [array names tid] {
				if {[lsearch $tid($id) [lindex $listvar 0]] != -1 && [lsearch $tid($id) [lindex $listvar 1]] != -1} {
				lappend listtmp $id
				}
			}
			append str [mc [lindex $listvar 0]] \040 [mc [lindex $listvar 1]]
		}

		3 {
			foreach id [array names tid] {
				if {[lsearch $tid($id) [lindex $listvar 0]] != -1 && [lsearch $tid($id) [lindex $listvar 1]] != -1 && [lsearch $tid($id) [lindex $listvar 2]] != -1} {
				lappend listtmp $id
				}
			}
			append str [mc [lindex $listvar 0]] \040 [mc [lindex $listvar 1]] \040 [mc [lindex $listvar 2]]
		}
	}

#set str "[mc {Voir tous les}] [string map {carre carr�red rouge gray75 gris green vert blue bleu yellow jaune cyan cyan magenta mauve brown marron} $str]"

set str "[mc {Voir tous les}] $str"
if {[llength $listtmp] == 0} {
append str [mc {: il n'y en a pas.}]
}
if {[llength $listtmp] > $visible} {
append str "[mc {: il y en a plus de}] $visible ."
}

if {[llength $listtmp] != 0 && [llength $listtmp] <= $visible} {
foreach item $listtmp {
$c itemconfigure $item -state normal
}
}

.mframe.consigne configure -text $str
set tforme null
set tcouleur null
set ttaille null
}

proc barre_nav {c} {
global criter0 criter1 criter2 sysFont
variable tforme
variable tcouleur
variable ttaille
set ext .gif
set compt 0
set tmp 0
if {[llength $criter0] != 1} {
foreach item $criter0 {
image create photo $item -file [file join sysdata $item$ext]
radiobutton .bframe.$item -image $item -activebackground grey -background grey -indicatoron 0 -width 30 -height 30 -variable tforme -value $item -command ".mframe.consigne configure -text \\040"
grid .bframe.$item -column $tmp  -padx 1 -pady 1 -row 0
incr compt
incr tmp
}
label .bframe.l1 -background #ffff80 -text " "
grid .bframe.l1 -column $tmp -row 0 -padx 2
incr tmp
} else {
set tforme null
}


if {[llength $criter1] != 1} {
set compt 0
foreach coul $criter1 {
radiobutton .bframe.$coul  -text "    " -activebackground $coul -background $coul -indicatoron 0 -selectcolor $coul -variable tcouleur -value $coul -command ".mframe.consigne configure -text \\040"
grid .bframe.$coul -column $tmp -row 0 -padx 1 -pady 1
incr compt
incr tmp
}
label .bframe.l2 -background #ffff80 -text " "
grid .bframe.l2 -column $tmp -row 0 -padx 2
incr tmp
} else {
set tcouleur null
}


if {[llength $criter2] != 1} {
set compt 0
foreach item $criter2 {
image create photo $item -file [file join sysdata $item$ext]
radiobutton .bframe.$item -image $item -activebackground grey -background grey -indicatoron 0 -width 30 -height 30 -variable ttaille -value $item -command ".mframe.consigne configure -text \\040"
grid .bframe.$item -column $tmp -row 0 -padx 1 -pady 1
incr compt
incr tmp
}
label .bframe.l3 -background #ffff80 -text " "
grid .bframe.l3 -column $tmp -row 0 -padx 2
incr tmp
} else {
set ttaille null
}


image create photo voir -file [file join sysdata voir$ext]
button .bframe.voir -image voir -activebackground grey -background grey -width 30 -height 30 -command "afficheitem $c"
grid .bframe.voir -column $tmp -row 0 -padx 1 -pady 1
incr tmp
label .bframe.l4 -background #ffff80 -text " "
grid .bframe.l4 -column $tmp -row 0 -padx 2
incr tmp

entry .bframe.reponse -width 3 -relief sunken -font $sysFont(l)
grid .bframe.reponse -column $tmp -row 0 -padx 1 -pady 1
incr tmp
image create photo rep -file [file join sysdata rep$ext]
button .bframe.rep -image rep -activebackground grey -background grey -width 30 -height 30 -command "verif $c"
grid .bframe.rep -column $tmp -row 0 -padx 1 -pady 1
incr tmp
label .bframe.tete -background #ffff80 -image neutre
grid .bframe.tete -column $tmp -row 0

}

#proc�ure principale
proc place {c} {
global listdata arg1 arg2 gettext listgen user criter0 criter1 criter2 total visible nbessai nbvoir reussite sysFont basedir progaide
set listdata [lindex $listgen $arg2]
set nbessai 0
set nbvoir 0
set reussite 0

wm title . "[mc $arg1] [lindex [lindex $listdata 0] 0]"
catch {destroy .bframe}

frame .bframe -background #ffff80
pack .bframe -side bottom

frame .mframe -background #ffff80
pack .mframe -side bottom
label .mframe.consigne -text [mc {Choisis une forme, une couleur, une taille puis clique sur le bouton voir, ou tape la reponse et appuie sur le bouton ok}] -background #ffff80 -font $sysFont(s)
pack .mframe.consigne -side left
set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"


###############################################################################################""
set compt 0
set listtmp [lindex [lindex $listdata 1] 0]
set listformes {cercle carre rectangle triangle}
set criter0 ""
foreach itm $listtmp {
if {[lindex $listtmp $compt] == 1} {
lappend criter0 [lindex $listformes $compt]
}
incr compt
}

set compt 0
set listtmp [lindex [lindex $listdata 1] 1]
set listcouleurs {gray75 red green blue  yellow cyan magenta  brown}
set criter1 ""
foreach itm $listtmp {
if {[lindex $listtmp $compt] == 1} {
lappend criter1 [lindex $listcouleurs $compt]
}
incr compt
}

set compt 0
set listtmp [lindex [lindex $listdata 1] 2]
set listtailles {petit grand}
set criter2 ""
foreach itm $listtmp {
if {[lindex $listtmp $compt] == 1} {
lappend criter2 [lindex $listtailles $compt]
}
incr compt
}

set listtmp0 $criter0
set listtmp1 $criter1
set listtmp2 $criter2
set listdiscri [lindex [lindex $listdata 1] 3]
set compt 0
foreach item $listdiscri {
if {$item == 0} { set listtmp$compt null}
incr compt
}

set arbreclasses ""
foreach item0 $listtmp0 {
foreach item1 $listtmp1 {
foreach item2 $listtmp2 {
lappend arbreclasses "$item0 $item1 $item2"
}
}
}
set tmpclasse [llength $arbreclasses]

set visible [lindex [lindex [lindex $listdata 1] 4] 1]
set min [lindex [lindex [lindex $listdata 1] 5] 1]
set max [lindex [lindex [lindex $listdata 1] 6] 1]


set nbtotal [expr int(rand()*($max-$min +1)) + $min]
set total $nbtotal
set minvisible [expr int(floor($nbtotal/[llength $arbreclasses]))]
set listpr ""
while {$nbtotal > 0 && [llength $arbreclasses] != 0} {
set nbvisibles [expr int(rand()*($visible-$minvisible +1)) + $minvisible]
if {$nbvisibles > $nbtotal} {set nbvisibles $nbtotal}
set ind [expr int(rand()*[llength $arbreclasses])]
set objet [lindex $arbreclasses $ind]

for {set i 1} {$i <= $nbvisibles} {incr i 1} {

	for {set j 0} {$j < [llength $objet]} {incr j 1} {
		if {[lindex $objet $j] == "null"} {
		set item [lindex [expr \$criter$j] [expr int(rand()*[llength [expr \$criter$j]])]]
		set objet [lreplace $objet $j $j $item]
		}
	}

lappend listpr $objet
}
set arbreclasses [lreplace $arbreclasses $ind $ind]
incr nbtotal [expr - $nbvisibles]
}

set total [expr $total - $nbtotal]

#on efface et on place les images, apr� avoir touill�
	#for {set i 0} {$i < [llength $listpr]} {incr i 1} {
	#set t1 [expr int(rand()*[llength $listpr])]
	#set t2 [expr int(rand()*[llength $listpr])]
	#set tmp [lindex $listpr $t1]
	#set listpr [lreplace $listpr $t1 $t1 [lindex $listpr $t2]]
	#set listpr [lreplace $listpr $t2 $t2 $tmp]
	#}

$c delete all
	for {set i 0} {$i < [llength $listpr]} {incr i 1} {
	set ypos [expr int($i/27)*23 +4]
	set xpos [expr int(fmod($i,27))*23 +4]
	createpolygon $c  \173[lindex $listpr $i]\175 $xpos $ypos
	}	

set str ""
set str "\173[mc {Nombre total d'objets :}]$total\175\040\173[mc {Nombre de classes :}]$tmpclasse\175\040\173[mc {Nombre maximum d'objets par classe :}]$visible\175"
enregistreeval $arg1 [lindex [lindex $listdata 0] 0] $str $user 
barre_nav $c
}


#appel de la proc�ure principale
place $c


proc verif {c} {
global total nbessai reussite
incr nbessai 
if {$total == [.bframe.reponse get]} {
incr reussite
$c itemconfigure poly -state normal
.bframe.tete configure -image bien
geresuite $c
} else {
.bframe.tete configure -image mal
if {$nbessai == 4} {
$c itemconfigure poly -state normal
geresuite $c
}
}
}

#a la fin de l'exercice
proc geresuite {c} {
global arg2 arg3 arg4 listgen Home reussite nbvoir reussite total nbessai user
catch {destroy .mframe}
catch {destroy .bframe}
frame .bframe -background #ffff80
pack .bframe -side bottom
if {$reussite == 1} {
label .bframe.consigne -text [mc {Bravo, c'est gagne!}] -background #ffff80
} else {
label .bframe.consigne -text "[mc {Le nombre de figures etait de}] $total ." -background #ffff80
}
pack .bframe.consigne -side left -padx 20
#grid .bframe.consigne -column 1 -padx 30 -row 0
if {$arg2 < [expr [llength $listgen] -1] && $arg3 == 0} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suite $c"
pack .bframe.suite -side left

}

set f [open [file join $Home reglages parcours.conf] "r"]
set listparcours [gets $f]
close $f

if {$arg4 < [expr [llength $listparcours] -1] && $arg3 == 1} {
button .bframe.suite -image [image create photo -file [file join sysdata suite.gif] ] -background #ff80c0 -command "suiteparcours $c $arg4"
pack .bframe.suite -side left
}

button .bframe.recommencer -image [image create photo -file [file join sysdata again2.gif] ] -background #ff80c0 -command "recommence $c"
pack .bframe.recommencer -side left
#label .bframe.reussite -background #ffff80 -text "Nombre d'erreurs : $reussite"
#grid .bframe.reussite -column 4 -padx 30 -row 0
set str [append str "\173[mc {Nombre de questions :}] $nbvoir\175\040\173[mc {Nombre d'essais :}] $nbessai\175\040\173[mc {Reussite :}] $reussite\175"]
appendeval $str $user 

}
